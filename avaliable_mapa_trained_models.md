
## MAPA trained models for entity detection

During MAPA we have trained different entity detection models based on Deep Learning, in particular
using state-of-the-art Transformers architecture.

As it is well-known, the key to make an AI model work as expected is the training data
(i.e. the amount and quality of examples that teach the model what to do).

In MAPA, different datasets have been collected and manually trained, for different languages
and application domains. Since the ambition of MAPA was to train models for up to 24 different
languages, and for many languages the data is scarce and hard to obtain, we have relied on 
novel semi-automatic ways of reusing the data from language-to-language, using machine translation
and manual-annotations projection. This way, a dataset for a single language can be projected
to all the other 24 languages. This technique has its limitations, such as translation errors or
mismatches in the annotation projections, that accumulate and may impact 
the final models performance compared to
using manually labelled data. But thanks to this effort, we have obtained a large base of 
trained models, that could not have been obtained by other means.

They sum up to **104** models.

Here is the general enumeration of models trained in MAPA, arranged by application domain 
and language (the performance may vary among domains and languages).

### Models trained without machine/translation/labels-projection

For the language/domains that had a good-enough native data, we provide trained models.
The whole relation of models can be browsed and downloaded 
from [**THIS LINK**](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150362056761)

(To download then, after click on the link, click the
download button in the top-right corner of the screen.
The model itself is the [uncompressed] whole folder with the long name that
contains the model-related files)

Language | Domain | Download
---|---|---
Spanish |  Legal | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150361145643)
French |  Legal  | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150361207182)
Spanish |  Clinical/Medical  | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150362056761)
French |  Clinical/Medical | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150362118780)
English |  Legal/Administrative  | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150361207182)
Greek |  Legal  | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150361068167)
Maltese | Legal | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150361314422)
Maltese | Administrative | [download](https://vicomtech.app.box.com/s/nqht3mb5lnbizxhl7p9nz175w5pd6a9u/folder/150361140105)

### Models trained using machine/translation/labels-projection

Using the machine-translation and label projection technique,
we have managed to train models for several domains in 24 languages.

**NOTE:** Individual links in the tables might not be updated, use the link to each general folder per domain, and simply browse for
the model of the language you want.

#### LEGAL domain models

The whole relation of models can be browsed/downloaded from
[**THIS LINK**](https://vicomtech.box.com/s/dksvz5s0kwg3mowl90mswr8y6rb3o7u7)

Language code | Download  
---|---
BG |  [download]()
CS | [download]()
DA |  [download]()
DE | [download]()
EL | [download]()
EN |  [download]()
ES |  [download]()
ET |  [download]()
FI |  [download]()
FR |  [download]()
GA |  [download]()
HR |  [download]()
HU |  [download]()
IT |  [download]()
LT |  [download]()
LV |  [download]()
MT |  [download]()
NL |  [download]()
PL |  [download]()
PT |  [download]()
RO |  [download]()
SK |  [download]()
SL |  [download]()
SV |  [download]()


#### CLINICAL domain models

The whole relation of models can be browsed/downloaded from
[**THIS LINK**](https://vicomtech.box.com/s/5e975fppfazivjbo5phchcwq2lgqqiw3)

Language code | Download  
---|---
BG |  [download]()
CS | [download]()
DA |  [download]()
DE | [download]()
EL | [download]()
EN |  [download]()
ES |  [download]()
ET |  [download]()
FI |  [download]()
FR |  [download]()
GA |  [download]()
HR |  [download]()
HU |  [download]()
IT |  [download]()
LT |  [download]()
LV |  [download]()
MT |  [download]()
NL |  [download]()
PL |  [download]()
PT |  [download]()
RO |  [download]()
SK |  [download]()
SL |  [download]()
SV |  [download]()

#### ADMINISTRATIVE domain models

The whole relation of models can be browsed/downloaded from
[**THIS LINK**](https://vicomtech.box.com/s/dsrtyc2yegklqcq2rph9412wry7h5mqz)

Language code | Download  
---|---
BG |  [download]()
CS | [download]()
DA |  [download]()
DE | [download]()
EL | [download]()
EN |  [download]()
ES |  [download]()
ET |  [download]()
FI |  [download]()
FR |  [download]()
GA |  [download]()
HR |  [download]()
HU |  [download]()
IT |  [download]()
LT |  [download]()
LV |  [download]()
MT |  [download]()
NL |  [download]()
PL |  [download]()
PT |  [download]()
RO |  [download]()
SK |  [download]()
SL |  [download]()
SV |  [download]()

#### NEWS/General domain models

The whole relation of models can be browsed/downloaded from
[**THIS LINK**](https://vicomtech.box.com/s/9egwhkt8m60j13o8ttwkug6j7hgahr84)

Language code | Download  
---|---
BG |  [download]()
CS | [download]()
DA |  [download]()
DE | [download]()
EL | [download]()
EN |  [download]()
ES |  [download]()
ET |  [download]()
FI |  [download]()
FR |  [download]()
GA |  [download]()
HR |  [download]()
HU |  [download]()
IT |  [download]()
LT |  [download]()
LV |  [download]()
MT |  [download]()
NL |  [download]()
PL |  [download]()
PT |  [download]()
RO |  [download]()
SK |  [download]()
SL |  [download]()
SV |  [download]()