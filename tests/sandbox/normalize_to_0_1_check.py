"""
Manual check
"""
import torch

t = torch.tensor([[-0.2, 0.3, 0.5, 1.7], [0.7, -0.8, 0.4, 1.2]])
print(t)

t -= t.min(dim=1, keepdim=True)[0]
t /= t.max(dim=1, keepdim=True)[0]

print(t)
