from transformers import AutoTokenizer, FlaubertTokenizer

from mapa_project.entity_detection.common.pretokenization_helpers import GeneralPreTokenizer

models_cache_dir = "/DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE"

# model_name = 'camembert/camembert-base'
model_name = 'flaubert/flaubert_base_cased'
# model_name = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/systematic_trainings/preliminary_tests/checkpoints/' \
#                  'eurlex_fr_v1_epoch27_iter840_L1_miF-0.3776_L1_binF-0.4436_L2_miF-0.4287_loss-0.3454/'

tokenizer = FlaubertTokenizer.from_pretrained(pretrained_model_name_or_path=model_name, cache_dir=models_cache_dir,
                                              do_lowercase_and_remove_accent=True, do_lower_case=True)

print('tokenizer.do_lowercase_and_remove_accent', tokenizer.do_lowercase_and_remove_accent)
print('tokenizer.do_lower_case', tokenizer.do_lower_case)

pretokenizer = GeneralPreTokenizer(prepend_whitespaces=True, remove_linebreaks=True, add_linebreak_tokens=False)

text = 'Le président Barack Obama a vécu à Washington D.C. and guachiponoreteidao.'
pretokens = pretokenizer.do_pretokenization(raw_text=text, calculate_offsets=False, as_list_of_tuples=False)

print(tokenizer.all_special_tokens)

for pretoken in pretokens:
    res = tokenizer.tokenize(pretoken)
    print(pretoken, '-->', res)
