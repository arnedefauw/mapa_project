import sys
import time
import logging
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler


class MyEventHandler(FileSystemEventHandler):

    def on_modified(self, event):
        super().on_modified(event)
        if event.is_directory:
            return
        with open(event.src_path, 'r', encoding='utf-8') as f:
            content = f.read()
        print(f'Woaaa {event.src_path} modified, now its content is: {content}')


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    # path = sys.argv[1] if len(sys.argv) > 1 else '.'
    path = '/DATA/agarciap_data/MAPA_STUFF/MAPA_FINAL/tests/dummy_file.txt'
    # event_handler = LoggingEventHandler()
    event_handler = MyEventHandler()
    observer = Observer()
    observer.start()
    observer.schedule(event_handler, path, recursive=True)
    try:
        while True:
            time.sleep(1)
    finally:
        observer.stop()
        observer.join()

