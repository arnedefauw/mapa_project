import os

import confuse
from confuse import Configuration


def test_config_confuse():
    config: Configuration = confuse.Configuration('MAPA', __name__)
    config.set_file(
        os.path.join(os.path.dirname(__file__), '../..', 'example_files', 'example_configuration', 'improved_config', 'mapa_config2.yaml'))

    value = config['showcase']['default_text_file'].get(str)

    print(value)

    value2 = config['detection_models'].get(dict).keys()
    print(value2)

    value3 = config['detection']['pattern_matching']
    value4 = value3['regex_files'].get(list)
    print(value4)
