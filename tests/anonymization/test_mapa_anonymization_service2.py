"""
The refactored anonymization service
"""
import pytest

from mapa_project.anonymization.datatypes_definition import MapaResult
from mapa_project.entity_detection.mapa_entity_detection import MapaEntityDetector, EntityDetectionConfig, PatternMatchingConfig
from mapa_project.anonymization.mapa_anonymization import MapaAnonymizer
from mapa_project.entity_replacement.mapa_entity_obfuscation import MapaEntityObfuscator, EntityObfuscationConfig
from mapa_project.entity_replacement.gazetteer_based_replacement import GazetteerReplacerConfig
from mapa_project.entity_replacement.general_random_replacer import GeneralRandomReplacerConfig
from mapa_project.entity_replacement.mapa_entity_replacement import MapaEntityReplacer, EntityReplacementConfig
from mapa_project.entity_replacement.neural_lm_entity_replacement import NeuralLMEntityReplacerConfig
from mapa_project.webservice.app.web_api_formats import AnonymizationOp
from tests import init_test_logger

init_test_logger()


@pytest.fixture
def anonymization_service_improved() -> MapaAnonymizer:
    entity_detection_config = EntityDetectionConfig(neural_model_path='/DATA/agarciap_data/MAPA_STUFF/MAPA_EXPERIMENTS/showcase_deployment/'
                                                                      'deployed_models_dtv3/'
                                                                      'eurlex_ALL_v3_epoch124_iter109375_L1_miF-0.8374_L1_binF-0.8669_L2_miF-0.8467_loss-6.2376/',
                                                    valid_seq_len=300,
                                                    ctx_window_len=100,
                                                    batch_size=4,
                                                    cuda_device_num=-1)
    pattern_matching_config = PatternMatchingConfig(named_patterns_files=None,
                                                    direct_named_patterns=None,
                                                    pattern_matching_enabled=True,
                                                    pattern_files_hot_reload=True)

    neural_replacer_config = NeuralLMEntityReplacerConfig(
        model_path='bert-base-multilingual-cased',
        # model_path='ixa-ehu/ixambert-base-cased',
        cache_dir='/DATA/agarciap_data/python_stuff/pytorch-torchtext-data/CACHE',
        replaceable_entity_types=['PERSON', 'ADDRESS', 'given name', 'given name - male', 'given name - female', 'family name', 'city', 'country',
                                  'day', 'month', 'year'
                                  ],
        lowercase_replaceable_entity_types=['month'],
        stopwords=['el', 'la', 'de', 'a', 'del', 'le'],
        reserved_words=[],
        batch_size=8,
        top_candidates=10,
        chunk_window_size=30,
        only_head_replacement_languages=[],
        random_seed=None,  # 42,
        cuda_device_num=-1,
        use_amp=True,
        use_non_contextual_sim_reweighting=True,
        contextual_reweighting_alpha=0.5)
    random_replacer_config = GeneralRandomReplacerConfig(
        replaceable_entities=[],
        reserved_tokens=['DNI']
    )
    gazetteer_replacer_config = GazetteerReplacerConfig()

    entity_replacement_config = EntityReplacementConfig(neural_entity_replacer_config=neural_replacer_config,
                                                        general_random_replacer_config=random_replacer_config,
                                                        gazetteer_replacer_config=gazetteer_replacer_config)
    entity_obfuscation_config = EntityObfuscationConfig(entities_to_obfuscate=['PERSON'])

    entity_detector = MapaEntityDetector(entity_detector_config=entity_detection_config, pattern_matching_config=pattern_matching_config)
    entity_replacer = MapaEntityReplacer(entity_replacer_config=entity_replacement_config)
    entity_obfuscator = MapaEntityObfuscator(config=entity_obfuscation_config)

    anonymizer = MapaAnonymizer(entity_detector=entity_detector, entity_replacer=entity_replacer, entity_obfuscator=entity_obfuscator)
    return anonymizer


def test_mapa_anonymization_service2(anonymization_service_improved: MapaAnonymizer):
    # text = 'El señor Pérez vive en Barcelona desde el 26 de marzo de 2005.'
    anon_op = AnonymizationOp.REPLACE
    print()
    texts = [
        'El señor Pérez vive en Barcelona desde el 26 de Marzo de 2005.',
        'Madrid, 3 de abril de 1997',
        'El señor Gerard Mas Pi vive en Donostia desde el 26 de Agosto de 2005.',
        'El expresidente George W. Bush es paisano de Texas, en los Estados Unidos.',
        'Un individuo no identificado, abordó a Gerard Mas Pi cuando transitaba tranquilamente por la calle Pavía de Barcelona, y tras sujetarle por la espalda al tiempo que la persona ignota le cogía por el cuello, le exigió que le entregara todo lo que llevaba, a lo que le víctima se resistió, ante lo cual el individuo desconocido le dio varios puñetazos en la cara hasta que la víctima cayó al suelo, momento en el que el acusado le propinó diversas patadas en la espalda y en la cara, logrando finalmente apoderarse de un teléfono móvil marca Nokia, modelo 6120 Classic, propiedad de Gerard Mas, tras lo cual se dio a la fuga.',
        """        Por todo lo expuesto, el Consejo de Estado es de dictamen: Que procede desestimar la reclamación de daños y perjuicios formulada por ... ." V.E., no obstante, resolverá lo que estime más acertado. Madrid, 3 de abril de 1997 EL SECRETARIO GENERAL, EL PRESIDENTE, EXCMA. SRA. MINISTRA DE EDUCACION Y CULTURA"""
    ]
    for text in texts:
        result: MapaResult = anonymization_service_improved.anonymize(text=text, anonymization_op=anon_op)

        print('Original text:', text)
        print('Replaced text:', result.text)
        print('Level1 entities:', result.level1_entities)
        print('Level2 entities:', result.level2_entities)
        print('Analysis time:', result.analysis_seconds)
        # print('Using GPU:', result.using_gpu)
        # print('Num tokens:', result.num_tokens)
        # print('Performing entity_replacement:', result.performing_replacement)
        print('=================')
