import logging


def init_test_logger():
    FORMAT = '%(asctime)s %(levelname)s (%(threadName)s) %(module)s: %(message)s'
    logging.basicConfig(format=FORMAT, level=logging.INFO)


init_test_logger()
