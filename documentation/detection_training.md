# MAPA entity detection

MAPA entity detection uses trained Deep Learning neural network models.
The performance of this type of models tends to be very high. However, they need to be trained 
on a reasonable amount of labelled data that resembles the content the model will need to analyse.

This repository provides functionality to train new models based on labelled data.
All you need is the proper hardware (a CUDA-capable GPU to accelerate the training) and the 
training data itself.
The data needs to be converted to a certain format in jsonlines.
During MAPA project, the manual labelling has been done using InCeption annotation tool, so we 
provide a functionality to convert the InCeption TSV files to the required jsonlines format.
However, if you have data in a different format, you can do the transformation on your own.
Read below.

## Converting InCeption TSV files to jsonlines

The training of the named entity recognition in MAPA needs the data in a specific (yet simple) format.
It is a jsonlines, in which each line is a separated document.
Each line thus, represents an instance with three main fields:
  - tokens: with is the sequence of tokens from the original text
  - level1_tags: since MAPA has two level of tags, this is the IOB sequence of entity tags for level1
  - level2_tags: same as level1_tags, but for the level2 according to MAPA hierarchy.

A dummy example of the jsonlines format:
```json
{"tokens": ["token1", "token2", "tokenN"], "level1_tags": ["tag1", "tag2", "tagN"], "level2_tags": ["tag1", "tag2", "tagN"]}
{"tokens": ["token1", "token2", "tokenN"], "level1_tags": ["tag1", "tag2", "tagN"], "level2_tags": ["tag1", "tag2", "tagN"]}
{"tokens": ["token1", "token2", "tokenN"], "level1_tags": ["tag1", "tag2", "tagN"], "level2_tags": ["tag1", "tag2", "tagN"]}
```

If you have labelled data from any other dataset of your own 
you can convert yourself the data into this format, there is nothing special with it.
If you have the InCeption annotation tool exported tsv files, we provide you with a command to 
make the conversion.

The command is *mapa_conversion* and its description is the following:

```text
usage: MAPA conversion from Inception TSV to JSONLINES [-h] [--train_folders TRAIN_FOLDERS] [--dev_folders DEV_FOLDERS] [--test_folders TEST_FOLDERS] --output_folder OUTPUT_FOLDER
                                                       --output_basename OUTPUT_BASENAME

optional arguments:
  -h, --help            show this help message and exit
  --train_folders TRAIN_FOLDERS
                        Paths to folders with TRAIN TSV files (if more than one, separate them by comma ",")
  --dev_folders DEV_FOLDERS
                        Paths to folders with DEV TSV files (if more than one, separate them by comma ",")
  --test_folders TEST_FOLDERS
                        Paths to folders with TEST TSV files (if more than one, separate them by comma ",")
  --output_folder OUTPUT_FOLDER
                        Target output folder to save the converted files
  --output_basename OUTPUT_BASENAME
                        The basename for the set of converted files (they will be appended with TRAIN, DEV or TEST to differentiate them)

```

You can plug several folder paths (separated by comma) to each of the groups (train, dev, test).
The files inside those folders with be converted and joined into a single file for each group.

## Training a MAPA entity-detection model

Training data is avalaible from the [ELRC platform](https://www.elrc-share.eu/repository/search/?q=MAPA)

First thing you need to train a new MAPA model is annotated data.
It is also required to have access to a CUDA capable GPU, otherwise the training would be
painfully slow.

The training functionality is rather complex, and despite having a lot of its internal hidden 
under abstractions or reasonable default values, it still needs several arguments to be
configured before the training can start.

The training command is *mapa_train* and has the following parameters.
Despite we have simplified it to the maximum, it is a complex command.
However, good news are that most of the arguments do not need to be changed and can work with reasonable 
defaults. We provide a simple [**EXAMPLE FILE**](../example_files/test_mapa_final_train.sh) 
that illustrates a minimal command launch.

```text
usage: mapa_train --model_name MODEL_NAME --model_version MODEL_VERSION [--model_description MODEL_DESCRIPTION] --pretrained_model_name_or_path PRETRAINED_MODEL_NAME_OR_PATH
                         [--pretrained_tokenizer_name_or_path PRETRAINED_TOKENIZER_NAME_OR_PATH] [--enable-do_lower_case] --models_cache_dir MODELS_CACHE_DIR --data_dir DATA_DIR --train_set
                         TRAIN_SET --dev_set DEV_SET --checkpoints_folder CHECKPOINTS_FOLDER [--max_checkpoints_to_keep MAX_CHECKPOINTS_TO_KEEP] [--num_epochs NUM_EPOCHS]
                         [--early_stopping_patience EARLY_STOPPING_PATIENCE] --batch_size BATCH_SIZE [--train_iters_to_eval TRAIN_ITERS_TO_EVAL]
                         [--gradients_accumulation_steps GRADIENTS_ACCUMULATION_STEPS] [--clip_grad_norm CLIP_GRAD_NORM] [--lr LR] [--warmup_epochs WARMUP_EPOCHS] [--cuda_devices CUDA_DEVICES]
                         [--disable-amp] [--random_seed RANDOM_SEED] [--valid_seq_len VALID_SEQ_LEN] [--ctx_len CTX_LEN] [--labels_to_omit LABELS_TO_OMIT] [--help] [--version]

optional arguments:
  --model_name MODEL_NAME
                        The name of the trained model (type:str default:None)
  --model_version MODEL_VERSION
                        The version number of the model (type:int default:None)
  --model_description MODEL_DESCRIPTION
                        An optional description of the model (type:str default:None)
  --pretrained_model_name_or_path PRETRAINED_MODEL_NAME_OR_PATH
                        The name or path to the pretrained base model (type:str default:None)
  --pretrained_tokenizer_name_or_path PRETRAINED_TOKENIZER_NAME_OR_PATH
                        Name/path to pretrained tokenizer, only in case it differs from the model name/path (type:str default:None)
  --enable-do_lower_case
                        Set do_lower_case to True
  --models_cache_dir MODELS_CACHE_DIR
                        Cache directory to store downloaded models (type:str default:None)
  --data_dir DATA_DIR   Directory containing the train/dev data (typing.Union[pathlib.Path, str] default:None)
  --train_set TRAIN_SET
                        Name of the training data file (type:str default:None)
  --dev_set DEV_SET     Name of the validation (a.k.a dev) data file (type:str default:None)
  --checkpoints_folder CHECKPOINTS_FOLDER
                        Directory to store the model checkpoints during the training (typing.Union[pathlib.Path, str] default:None)
  --max_checkpoints_to_keep MAX_CHECKPOINTS_TO_KEEP
                        Number of checkpoints to keep during training (the oldest ones are removed) (type:int default:None)
  --num_epochs NUM_EPOCHS
                        The maximum number of epochs to train (type:int default:200)
  --early_stopping_patience EARLY_STOPPING_PATIENCE
                        Number of epochs without improvement before early-stopping the training (type:int default:50)
  --batch_size BATCH_SIZE
                        The batch size (number of examples processed at once) during the training (type:int default:None)
  --train_iters_to_eval TRAIN_ITERS_TO_EVAL
                        The number of iterations before triggering an evaluation (apart from full epochs) (type:int default:-1)
  --gradients_accumulation_steps GRADIENTS_ACCUMULATION_STEPS
                        The number of steps to accumulate gradients before an optimizer step (type:int default:1)
  --clip_grad_norm CLIP_GRAD_NORM
                        The value for gradient clipping (type:float default:1.0)
  --lr LR               The learning rate for the training (type:float default:2e-05)
  --warmup_epochs WARMUP_EPOCHS
                        The number of epochs to warmup the learning rate. Admits non-integer values, e.g. 0.5 (type:float default:1.0)
  --cuda_devices CUDA_DEVICES
                        Comma-separated CUDA device numbers to train on GPU (-1 means CPU) (type:str default:-1)
  --disable-amp         Set amp to False
  --random_seed RANDOM_SEED
                        The random seed, to make the experiments reproducible (type:int default:42)
  --valid_seq_len VALID_SEQ_LEN
                        The length of the sliding-window (the central valid part excluding the contexts) (type:int default:300)
  --ctx_len CTX_LEN     The length of the context surrounding each sliding-window (type:int default:100)
  --labels_to_omit LABELS_TO_OMIT
                        The labels omitted from the in-training evaluation (default for BERT) (type:str default:O,X,[CLS],[SEP],[PAD])
  --help                Print Help and Exit
  --version             show program's version number and exit
```

If you manage to run a training, check that a GPU is being used.
Otherwise the training will be painfully slow.

The trained model checkpoints (those that obtain a better validation score in the dev set) will
be stored in the checkpoints folder configured in the command.

Once you have a trained model checkpoint, you can use it as part of the launch-configuration.

### Using a YAML file to configure the training

Since the training command has so many potential arguments, it is very cumbersome (and prone to errors) 
to write them down directly in console. Writing a console script is also a bit dirty and difficult to
maintain, so in order to ease the training configuration there is also the option to write down
the configuration in a YAML file and use it as the input to the training command.

The YAML file is a simple one-level listing of the very same arguments, but they can be commented,
documented and maintained more easily. You can find an example of a YAML training configuration 
file at [here](/example_files/single_train_config_example.yaml).

In order to use the yaml file for training, you need to use a slightly different command:

```bash
mapa_train_with_cfg --config my/yaml/config.yaml
```

The training and the rest of the process (including the semantics of the arguments) should remain
the same.
