![alt text](https://mapa-project.eu/wp-content/uploads/2020/03/cropped-logo-1.png "MAPA Anonymisation")

# MAPA anonymization

[MAPA](https://mapa-project.eu/) is an integration project that aims at introducing 
Natural Language Processing (NLP) tools and 
at developing a toolkit for anonymization of texts.

This repository contains the code providing the following functionalities:
  - Train an artificial intelligence model for entity detection using labelled data
  - Configure and deploy the different assets that take part in the anonymization
  - Launch a service, including a simple html interface to try out the anonymization

**Disclaimer:** *At the time of this writing this is still work in progress. Some functionalities might
not be fully implemented or contain some bugs. There might be changes that render part of this 
documentation outdated.*

## License

[Apache-2.0](https://www.apache.org/licenses/LICENSE-2.0)

## Quick start 

### 1. Intro

The goal of this "quick start" demo is to create and deploy a simple anonymization service.

The MAPA project has developed anonymization models for all European languages in many domains. In this demo, we will use some of them:
Spanish, English, French, German, Maltese, Irish, Latvian, and a multilingual model for all European languages.

### 2. Requirements 
  - OS: Ubuntu.
  - You should have [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04), [Docker-compose](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04) and [Git](https://phoenixnap.com/kb/how-to-install-git-on-ubuntu) installed.

### 3. Clone the MAPA repository

```BASH
git clone https://gitlab.com/MAPA-EU-Project/mapa_project
```

### 4. Download models

  - At the same level as the cloned repository (created in the previous step), create a new folder named **mapa_data**.
```BASH
mkdir mapa_data
```  
  - Download one or more models using the links in the table below.
  - **Unzip** them, and then put them in the **mapa_data** directory created previously.

Language | Domain | Download  
---|---|---
Spanish |  Legal | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/es.zip)
English | Administrative | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/en.zip)
French | Medical | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/fr.zip)
German | Legal | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/de.zip)
Maltese | Administrative | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/mt.zip)
Irish | Legal | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/ga.zip)
Latvian | Legal | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/lv.zip)
Multilingual | Administrative | [download](https://s3.eu-west-1.amazonaws.com/com.pangeanic.mapa/multilingual.zip)

**_**A more exhaustive trained models relation can be found at [available mapa trained models](avaliable_mapa_trained_models.md)**_**.

### 5. Build the docker
  - We will now build an anonymization service for one of the models we have downloaded previously, the **multilingual** one, as an example. 

```
my-project
│
└───mapa_project
│   │   ...
│   │   docker-compose-quick-start.yml
│   │   ...

└───mapa_data
│   │   ...
│   │   multilingual
│   │   ...
```

  - First, we need to edit the **docker-compose-quick-start.yml** file (line 18), and replace XXX with the **absolute** path of the **multilingual** directory.
  - Then, we can build the docker:

```BASH
sudo docker-compose -f docker-compose-quick-start.yml build
```

### 5. Start the anonymization service

**NOTE:** *to load the models in GPU (for faster computation) you need to edit the mapa_config.yaml
file in the models folder, and set the **cuda_device** value to a valid GPU id (-1 means using CPU).
Each model can be loaded in a different device (each one has its own cuda_device value).*

```BASH
sudo docker-compose -f docker-compose-quick-start.yml up
```

### 6. Usage
  - The simplest way to test the service is to connect to the following web panel:

[http://0.0.0.0:8671/mapa/1.0/anonymization/model_showcase](http://0.0.0.0:8671/mapa/1.0/anonymization/model_showcase)

  - Through API:
```BASH
curl -X 'POST' \
  'http://0.0.0.0:8671/mapa/1.0/anonymization/anonymize_text' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "text": "El señor Pérez.",
  "operation": "REPLACE"
}'
```
Result:
```JSON
{
  "anonymized_text": "El señor Connelly.",
  "annotations": [
    {
      "content": "señor Connelly",
      "begin": 3,
      "end": 17,
      "value": "PERSON"
    },
    {
      "content": "señor",
      "begin": 3,
      "end": 8,
      "value": "title"
    },
    {
      "content": "Connelly",
      "begin": 9,
      "end": 17,
      "value": "family name"
    }
  ],
  "elapsed_seconds": 0.7789840698242188,
  "words_per_second": 3.851169897064729,
  "used_device": "CPU"
}
```

## Sections

- An [**overall description**](./documentation/components/mapa_components.md) 
of the modules and submodules for entity detection and replacement.


- Entity detection: MAPA uses Deep Learning (BERT-based) models for entity detection. The performance 
of the detection depends on the volume and quality of the training data for a given language and/or
  use-case. If you have manually labelled data of your own, you can train your own model. 
  [SETUP](documentation/project_setup.md) |
  [TRAINING](documentation/detection_training.md) | [REGEX](documentation/regex_module.md) | [Built-in evaluation](documentation/detection_evaluation.md) 
(just a utility for quick evaluation, the actual exhaustive evaluation is done externally)
  

- Entity replacement: MAPA also tries to replace the detected entities by other, ideally equivalent, words 
that keep the meaning of the document close to the original while removing the sensitive information. 
  For that, MAPA uses a combination of a neural-language-model, a randomizer and a dictionary based 
  approach. All of them are configurable. [ENTITY REPLACEMENT](documentation/entity_replacement.md)

  
- Launching the service/demo: all the configuration, models and auxiliary files must be put together 
in a configuration file before launching the service/demo. It comes with Docker-related files to 
  ease its deployment. 
  [MAPA CONFIG](documentation/mapa_configuration.md) | [LAUNCHING SERVICE](documentation/launching_service.md)
