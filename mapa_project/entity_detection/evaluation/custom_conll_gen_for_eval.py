"""
This is to generate CoNLL alike outputs to be evaluated, as requested by LIMSI.
The input must be a list of "results", each one with a sequence of tokens and labels.
Optionally there might be gold-labels (if they were available).
The format is, one token per line, tab-separated columns, line-feed and the end, empty line to separate segments such as paragraphs (when possible).
Each line should be:
TOKEN <TAB> LEVEL1_GOLD <TAB> LEVEL2_GOLD <TAB> LEVEL1_PRED <TAB> LEVEL2_PRED <TAB> token_start:token_end <LINE_FEED>

Token span is just a pair of numbers indicating the character offset span (if available), e.g. 150:154

When a field is not available (gold labels, spans, etc.) they still need to be there, empty (the tabs must match).
To make the file more readable in those cases, the "empty" columns should bear a '-' as marker.
"""
from dataclasses import dataclass
from typing import List, Tuple, Optional


@dataclass
class ModelDescriptors:
    """ This class should contain the different variations for a model, and print them in a compact manner to easily identify the experiment """
    # It is not meant to represent all the hyper-parameters exhaustively (like the model-training-config), just an end-user friendly description.
    dataset_name: str  # any extra recognisable name if the dataset is a publicly known one (e.g. MEDDOCAN)
    dataset_domain: str  # a label for the domain (e.g. LEGAL, MEDICAL, etc.)
    dataset_lang: str  # the lang of the training data, or "multi"
    pretrained_model_lang: str  # the lang or "multi"
    cased: bool  # whether the pretrained model is cased or uncased
    training_seed: int  # the random seed used during training
    # In addition, about the model architecture, despite there is only a valid one for now
    mapa_model_arch: str  # a label, e.g. mapaV1 (to name the current standard e2FL)
    tagging_scheme: str  # IOB / IOBES

    def to_label(self, omit_field_name: bool = False):
        if omit_field_name:
            return '::'.join([f'{val}' for key, val in vars(self).items()])
        else:
            return '::'.join([f'{key}={val}' for key, val in vars(self).items()])


class CustomMAPACoNLLFormatter:

    def __init__(self):
        pass

    @classmethod
    def format_document(cls, model_descriptors: ModelDescriptors, tokens: List[str], token_spans: Optional[List[Tuple[int, int]]],
                        level1_golds: Optional[List[str]], level2_golds: Optional[List[str]],
                        level1_preds: List[str], level2_preds: List[str]) -> List[str]:
        resulting_lines: List[str] = [f'#{model_descriptors.to_label()}\n']
        resulting_lines += [f'TOKEN\tLEVEL1_GOLD\tLEVEL2_GOLD\tLEVEL1_PRED\tLEVEL2_PRED\tTOKEN_SPAN\n']
        level1_golds = level1_golds or ['-'] * len(tokens)
        level2_golds = level2_golds or ['-'] * len(tokens)
        for i, token in enumerate(tokens):
            if token == '':
                line = '\n'
            else:
                token_start, token_end = token_spans[i] if token_spans else ('-', '-')
                token_span_str = '-' if token_start == '-' else f'{str(token_start)}:{str(token_end)}'

                line = f'{token.strip()}\t{level1_golds[i]}\t{level2_golds[i]}\t{level1_preds[i]}\t{level2_preds[i]}\t{token_span_str}\n'
            resulting_lines.append(line)

        # avoid more than one consecutive empty line
        resulting_lines = [line for i, line in enumerate(resulting_lines) if not (i > 0 and line == '\n' and resulting_lines[i - 1] != '\n')]

        return resulting_lines

    # def format_documents(self, seq_labelling_outputs: List[MapaSeqLabellingOutput], doc_names:List[str]):
    #     doc_lines:List[List[str]] = []
    #     for output in seq_labelling_outputs:
    #         tokens = output.tokens
