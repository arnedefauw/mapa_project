from torch.nn import Module
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LambdaLR
from transformers import AdamW, get_linear_schedule_with_warmup, get_constant_schedule

from mapa_project.entity_detection.training.entity_detection_trainer_config import MapaEntityDetectionTrainerConfig


def instantiate_adamw(model: Module, lr: float, correct_bias=False):
    optimizer_grouped_parameters = __get_parameters_for_the_optimizer(model)
    optimizer = AdamW(optimizer_grouped_parameters, lr=lr, correct_bias=correct_bias)
    return optimizer


def __get_parameters_for_the_optimizer(model):
    optimizer_grouped_parameters = __model_parameters_for_full_finetuning(model)
    return optimizer_grouped_parameters


def __model_parameters_for_full_finetuning(model):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'gamma', 'beta']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
         'weight_decay_rate': 0.0}
    ]
    return optimizer_grouped_parameters


def instantiate_linear_scheduler_with_warmup(optimizer: Optimizer, num_training_instances: int, config: MapaEntityDetectionTrainerConfig) -> LambdaLR:
    warmup_steps = __calculate_warmup_steps(warmup_epochs=config.warmup_epochs, num_training_instances=num_training_instances,
                                            batch_size=config.batch_size, gradient_accumulation_steps=config.gradients_accumulation_steps)
    total_steps = __calculate_total_steps(total_epochs=config.num_epochs, num_training_instances=num_training_instances, batch_size=config.batch_size,
                                          gradient_accumulation_steps=config.gradients_accumulation_steps)
    lambda_lr = get_linear_schedule_with_warmup(optimizer=optimizer, num_warmup_steps=warmup_steps, num_training_steps=total_steps, last_epoch=-1)
    return lambda_lr


def instantiate_constant_lr_scheduler(optimizer: Optimizer) -> LambdaLR:
    return get_constant_schedule(optimizer=optimizer, last_epoch=-1)


def __calculate_warmup_steps(warmup_epochs: float, num_training_instances: int, batch_size: int, gradient_accumulation_steps: int) -> int:
    # Each epoch consists of num_training_instances/batch_size training steps
    real_batch_size = batch_size * gradient_accumulation_steps
    training_steps_per_epoch = num_training_instances / real_batch_size
    warmup_steps = int(warmup_epochs * training_steps_per_epoch)
    return warmup_steps


def __calculate_total_steps(total_epochs: int, num_training_instances: int, batch_size: int, gradient_accumulation_steps: int) -> int:
    real_batch_size = batch_size * gradient_accumulation_steps
    training_steps_per_epoch = num_training_instances / real_batch_size
    total_steps = int(total_epochs * training_steps_per_epoch)
    return total_steps


def __calculate_num_hard_reset_cycles(warmup_steps: int, epochs_per_cycle: float, total_epochs: int, num_training_instances: int,
                                      batch_size: int, gradient_accumulation_steps: int) -> int:
    real_batch_size = batch_size * gradient_accumulation_steps
    training_steps_per_epoch = num_training_instances / real_batch_size
    steps_per_reset_cycle = training_steps_per_epoch * epochs_per_cycle
    total_steps = total_epochs * training_steps_per_epoch
    steps_after_warmup = total_steps - warmup_steps
    num_hard_resets = int(steps_after_warmup / steps_per_reset_cycle)
    return num_hard_resets
