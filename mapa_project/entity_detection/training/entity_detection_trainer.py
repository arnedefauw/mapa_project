import logging
import os

from typing import Dict, List, Any, Union

import torch
from torch.cuda.amp import GradScaler
from torch.nn import Module
from torch.optim import Optimizer
from torch.optim.lr_scheduler import LambdaLR
from torch.utils.data.dataset import Dataset
from torch.utils.data.dataloader import DataLoader
from tqdm import tqdm
from transformers import AutoTokenizer, PreTrainedTokenizer, PreTrainedModel

from mapa_project.data_processing.mapa_hierarchy import MAPA_ENTITIES_HIERARCHY
from mapa_project.entity_detection.common.data_structures import DatasetsVocabsAndExtraResources, ModelStepOutput
from mapa_project.entity_detection.common.dataset_loading import TwoFlatLevelsDataset
from mapa_project.entity_detection.common.entity_detection_model import EnhancedTwoFlatLevelsSequenceLabellingConfig, \
    EnhancedTwoFlatLevelsSequenceLabellingModel
from mapa_project.entity_detection.common.field_definition import FieldDefinition
from mapa_project.entity_detection.common.misc_logic import set_all_random_seeds_to, use_cuda, to_data_parallel, is_multigpu_training, \
    batch_progress_bar, report_to_progress_bar, get_learning_rate, preinstantiation_stuff
from mapa_project.entity_detection.common.model_saving_loading import ModelSaver, ConditionalCheckpointSaver
from mapa_project.entity_detection.training.entity_detection_trainer_config import MapaEntityDetectionTrainerConfig
from mapa_project.entity_detection.training.optimizer_and_lr_scheduling import instantiate_adamw, instantiate_linear_scheduler_with_warmup, \
    instantiate_constant_lr_scheduler
from mapa_project.entity_detection.training.training_evaluation import EvaluationMetricsHolder, SimpleEvaluationMetricsAccumulator, \
    BaseEvaluationMetricsAccumulator, FscoreMetric, SequenceLabellingPostProcessor, EvaluationMetricCombination

logger: logging.Logger = logging.getLogger(__name__)


class MapaEntityDetectionTrainer:

    def __init__(self, config: MapaEntityDetectionTrainerConfig):
        preinstantiation_stuff()
        self.cfg: MapaEntityDetectionTrainerConfig = config
        logger.info(self.cfg.pretty_print_config())
        if self.cfg.random_seed and self.cfg.random_seed > 0:
            set_all_random_seeds_to(self.cfg.random_seed)
        else:
            logger.warning('No random seed has been configured. The training will be non-deterministic.')

        self.tokenizer = self.load_tokenizer(self.cfg)
        self.tokens_field = FieldDefinition.create_sequential_field('tokens', multilabel=False, tokenizer=self.tokenizer)
        self.level1_tags_field = FieldDefinition.create_sequential_field('level1_tags', multilabel=False, tokenizer=self.tokenizer, default_label='O')
        self.level2_tags_field = FieldDefinition.create_sequential_field('level2_tags', multilabel=False, tokenizer=self.tokenizer, default_label='O')

        datasets_vocabs_and_extra_resources: DatasetsVocabsAndExtraResources = self.define_fields_datasets_and_resources(config=self.cfg,
                                                                                                                         tokenizer=self.tokenizer)
        self.train_dataset: Dataset = datasets_vocabs_and_extra_resources.train_dataset
        self.dev_dataset: Dataset = datasets_vocabs_and_extra_resources.dev_dataset
        self.data_fields: Dict[str, FieldDefinition] = self.__create_data_fields_inventory(datasets_vocabs_and_extra_resources.data_fields)
        self.extra_resources: Dict[str, Any] = datasets_vocabs_and_extra_resources.extra_resources

        self.model = self.instantiate_model(self.cfg)
        self.use_amp = self.cfg.amp and use_cuda(self.cfg.cuda_devices)
        self.model = to_data_parallel(self.model, cuda_devices=self.cfg.cuda_devices, use_amp=self.use_amp)

        # Not sure about this, it is missing the check for more than a single GPU, but it has been working all the time
        self.is_multigpu_setting = is_multigpu_training(self.cfg.cuda_devices)

        self.optimizer = self.instantiate_optimizer(config=self.cfg, model=self.model)
        # NOTE: Pytorch Dataset no longer has a default __len__ (see their note...), but we can assume the subclasses will
        self.lr_scheduler = self.instantiate_lr_scheduler(optimizer=self.optimizer, config=self.cfg, num_training_instances=len(self.train_dataset))

        self.metrics_holder: EvaluationMetricsHolder = self.instantiate_evaluation_metrics(self.cfg)
        self.train_evaluation_metrics_accumulator = self.instantiate_evaluation_metric_accumulator(evaluation_metrics_holder=self.metrics_holder)
        self.dev_evaluation_metrics_accumulator = self.instantiate_evaluation_metric_accumulator(evaluation_metrics_holder=self.metrics_holder)

        self.conditional_checkpoint_saver = self.instantiate_conditional_checkpoint_saver(self.cfg, tokenizer=self.tokenizer)
        self.gradients_accumulation_steps = self.cfg.gradients_accumulation_steps

        # New, for automatic mixed precision (AMP), see: https://pytorch.org/docs/stable/notes/amp_examples.html
        self.scaler = GradScaler(enabled=self.use_amp)

    @classmethod
    def load_tokenizer(cls, config: MapaEntityDetectionTrainerConfig) -> PreTrainedTokenizer:
        """
        Overridable method in case we want to use a different tokenizer for a particular task.
        Note that if you use your tokenizer you must take care (in your data-loading) of the input-vocabulary generation
        """
        do_lower_case = 'uncased' in config.pretrained_model_name_or_path or config.do_lower_case

        tokenizer_name_or_path = config.pretrained_model_name_or_path
        if config.pretrained_tokenizer_name_or_path:
            logger.info(f'Tokenizer will be loaded from specified name/path: {config.pretrained_tokenizer_name_or_path}')
            tokenizer_name_or_path = config.pretrained_tokenizer_name_or_path
        else:
            logger.info(f'Tokenizer will be loaded from the same name/path than the pre-trained model')

        logger.info(f'Loading pre-trained TOKENIZER: >> {tokenizer_name_or_path} << (do_lower_case={do_lower_case})')
        tokenizer = AutoTokenizer.from_pretrained(pretrained_model_name_or_path=tokenizer_name_or_path, do_lower_case=do_lower_case,
                                                  cache_dir=config.models_cache_dir, output_loading_info=True
                                                  )
        return tokenizer

    def instantiate_optimizer(self, config: MapaEntityDetectionTrainerConfig, model: Module) -> Optimizer:
        """
        Instantiate the optimizer, by default an AdamW optimizer. Override this method if you want to use a custom optimizer
        """
        logger.info(f'Instantiating and AdamW optimizer')
        return instantiate_adamw(model=model, lr=config.lr, correct_bias=False)

    def instantiate_lr_scheduler(self, optimizer: Optimizer, config: MapaEntityDetectionTrainerConfig, num_training_instances: int) -> LambdaLR:
        """
        Instantiate the LR Scheduler. Override this method if you want to use a custom LR Scheduler
        """
        if config.warmup_epochs and config.warmup_epochs > 0:
            logger.info(f'Instantiating LR Scheduler with linear warmup of {config.warmup_epochs} epochs')
            return instantiate_linear_scheduler_with_warmup(optimizer=optimizer, num_training_instances=num_training_instances, config=config)
        else:
            logger.info(f'Instantiating constant LR Scheduler')
            return instantiate_constant_lr_scheduler(optimizer=optimizer)

    @classmethod
    def instantiate_evaluation_metric_accumulator(cls, evaluation_metrics_holder: EvaluationMetricsHolder) -> BaseEvaluationMetricsAccumulator:
        return SimpleEvaluationMetricsAccumulator(evaluation_metrics_holder.evaluation_metrics)

    def instantiate_conditional_checkpoint_saver(self, config: MapaEntityDetectionTrainerConfig,
                                                 tokenizer: PreTrainedTokenizer) -> ConditionalCheckpointSaver:
        model_saver = ModelSaver(base_folder=config.checkpoints_folder,
                                 model_name=f'{config.model_name}', model_version=config.model_version,
                                 data_fields=self.data_fields, extra_resources=self.extra_resources, tokenizer=tokenizer,
                                 training_config=config)
        return ConditionalCheckpointSaver(model_saver=model_saver,
                                          conditioning_metric_name=self.metrics_holder.metric_to_check.name,
                                          maximize_metric=self.metrics_holder.maximize_checked_metric,
                                          early_stopping_patience=config.early_stopping_patience,
                                          max_checkpoints_to_keep=config.max_checkpoints_to_keep)

    def __create_data_fields_inventory(self, data_fields: List[FieldDefinition]) -> Dict[str, FieldDefinition]:
        all_data_fields_map: Dict[str, FieldDefinition] = {}
        for data_field in data_fields:
            # add them to the self attributes (it is a bit dangerous if the name is not a valid identifier
            # the only value for this would be to declare in the child class to have code completion... but I am not sure if if would work
            self.__setattr__(data_field.name, data_field)
            all_data_fields_map[data_field.name] = data_field
        return all_data_fields_map

    def define_fields_datasets_and_resources(self, config: MapaEntityDetectionTrainerConfig,
                                             tokenizer: PreTrainedTokenizer) -> DatasetsVocabsAndExtraResources:
        """ Call the data-loading logic to load both train and dev sets, and create/load the related vocabs too """
        mapa_entities = MAPA_ENTITIES_HIERARCHY

        train_set_path = os.path.join(config.data_dir, config.train_set)
        train_dataset = TwoFlatLevelsDataset.load_from_file(input_path=train_set_path,
                                                            tokenizer=tokenizer, input_field=self.tokens_field,
                                                            level1_tags_field=self.level1_tags_field,
                                                            level2_tags_field=self.level2_tags_field,
                                                            valid_seq_len=config.valid_seq_len, ctx_len=config.ctx_len,
                                                            build_vocabs=True, entities_hierarchy=mapa_entities, train_subwords=True)

        dev_set_path = os.path.join(config.data_dir, config.dev_set)
        dev_dataset = TwoFlatLevelsDataset.load_from_file(input_path=dev_set_path,
                                                          tokenizer=tokenizer, input_field=self.tokens_field,
                                                          level1_tags_field=self.level1_tags_field,
                                                          level2_tags_field=self.level2_tags_field,
                                                          valid_seq_len=config.valid_seq_len, ctx_len=config.ctx_len,
                                                          build_vocabs=False, entities_hierarchy=None, train_subwords=True)

        dataset_vocabs_and_extras = DatasetsVocabsAndExtraResources(
            data_fields=[self.tokens_field, self.level1_tags_field, self.level2_tags_field],
            train_dataset=train_dataset, dev_dataset=dev_dataset,
            extra_resources={'mapa_entities': mapa_entities.to_json()})
        return dataset_vocabs_and_extras

    def instantiate_model(self, config: MapaEntityDetectionTrainerConfig) -> Union[Module, PreTrainedModel]:
        """ Instantiate/Load the model """
        # calling it model_config to avoid confusion with the trainer config (the input parameter)
        model_config = EnhancedTwoFlatLevelsSequenceLabellingConfig.from_pretrained(config.pretrained_model_name_or_path,
                                                                                    cache_dir=config.models_cache_dir)
        model_config.num_level1_labels = len(self.level1_tags_field.vocab)
        model_config.num_level2_labels = len(self.level2_tags_field.vocab)

        print(f'Loading pre-trained BERT with (enhanced) '
              f'two-levels token classification for MAPA with {model_config.num_level1_labels} and {model_config.num_level2_labels}'
              f' classes for each level respectively')
        model = EnhancedTwoFlatLevelsSequenceLabellingModel.from_pretrained(
            config.pretrained_model_name_or_path,
            cache_dir=config.models_cache_dir,
            config=model_config)

        # In case the line_break token has been added to the tokenizer increasing the vocab size
        model.resize_token_embeddings(len(self.tokenizer))

        return model

    def model_step(self, config: MapaEntityDetectionTrainerConfig, model: Module, batch, batch_number: int) -> ModelStepOutput:
        """
        A model step using an input batch, returning the loss, a dictionary of predictions (and scores), and a dictionary of gold labels
        :param config: the training configuration in case some value is needed to perform the model step
        :param model: the model to perform the step on
        :param batch: the input batch
        :param batch_number: the batch number
        :return: the loss, and an arbitrary dictionary of predictions (optionally scores) and gold labels. Dict keys must be the pairing vocab names.
        """
        inputs = batch[self.tokens_field.name]
        ctx_mask = batch[self.tokens_field.name + '_ctx_mask']
        attention_mask = batch[self.tokens_field.attn_mask_name()]
        level1_labels = batch[self.level1_tags_field.name]
        level2_labels = batch[self.level2_tags_field.name]

        outputs = model(inputs, attention_mask=attention_mask, ctx_mask=ctx_mask, level1_labels=level1_labels, level2_labels=level2_labels)
        loss, level1_labels_logits, level2_labels_logits = outputs[:3]
        model_step_output = ModelStepOutput(loss=loss,
                                            prediction_scores={self.level1_tags_field.name: level1_labels_logits,
                                                               self.level2_tags_field.name: level2_labels_logits},
                                            gold_labels={self.level1_tags_field.name: level1_labels, self.level2_tags_field.name: level2_labels})
        return model_step_output

    def instantiate_evaluation_metrics(self, config: MapaEntityDetectionTrainerConfig) -> EvaluationMetricsHolder:
        """ Instantiate the corresponding evaluation metrics and how their role during training """
        level1_labels_to_eval = [self.level1_tags_field.stoi(label) for label in self.level1_tags_field.vocab.keys()
                                 if label not in config.labels_to_omit.split(',')]
        level2_labels_to_eval = [self.level2_tags_field.stoi(label) for label in self.level2_tags_field.vocab.keys()
                                 if label not in config.labels_to_omit.split(',')]

        # Note the hardcoded -100 as the loss ignore index (this should work as-is both when subwords are trained and when not)
        level1_binarization_negative_labels = [self.level1_tags_field.stoi(tag) for tag in config.labels_to_omit.split(',')] + [-100]

        level1_microf1 = FscoreMetric(name='L1_miF', target_field=self.level1_tags_field,
                                      preprocessor=SequenceLabellingPostProcessor(pad_idx=self.level1_tags_field.pad_idx(), ctx_len=config.ctx_len),
                                      labels_to_evaluate=level1_labels_to_eval, average_method='micro')

        level1_binf1 = FscoreMetric(name='L1_binF', target_field=self.level1_tags_field,
                                    preprocessor=SequenceLabellingPostProcessor(pad_idx=self.level1_tags_field.pad_idx(), ctx_len=config.ctx_len),
                                    binarization_negative_labels=level1_binarization_negative_labels,
                                    average_method='binary')

        level2_microf1 = FscoreMetric(name='L2_miF', target_field=self.level2_tags_field,
                                      preprocessor=SequenceLabellingPostProcessor(pad_idx=self.level2_tags_field.pad_idx(), ctx_len=config.ctx_len),
                                      labels_to_evaluate=level2_labels_to_eval, average_method='micro')

        l1_l2_f1_comb = EvaluationMetricCombination(name='all_metrics_comb', metrics_to_combine=(level1_microf1, level2_microf1))

        evaluation_metrics_holder = EvaluationMetricsHolder(evaluation_metrics=[level1_microf1, level1_binf1, level2_microf1, l1_l2_f1_comb],
                                                            metric_to_check=l1_l2_f1_comb, maximize_checked_metric=True,
                                                            metrics_in_progress_bar=[level1_microf1, level1_binf1, level2_microf1],
                                                            metrics_in_checkpoint_name=[level1_microf1, level1_binf1, level2_microf1])
        return evaluation_metrics_holder

    ##########################
    # Core training methods (not meant to be overridden)
    ##########################

    def __load_train_dataloader(self) -> DataLoader:
        train_dataloader = DataLoader(
            dataset=self.train_dataset,
            batch_size=self.cfg.batch_size,
            shuffle=True
        )
        return train_dataloader

    def __load_dev_dataloader(self) -> DataLoader:
        dev_dataloader = DataLoader(
            dataset=self.dev_dataset,
            batch_size=self.cfg.batch_size,
            shuffle=False
        )
        return dev_dataloader

    def __iteration_based_evaluation_active(self) -> bool:
        """ As opposed to the typical full-epoch based evaluation """
        return self.cfg.train_iters_to_eval > 0

    def __should_evaluate(self, current_training_iter: int, full_epoch_finished: bool):
        """ If train_iters_to_eval is set, check if this iteration matches, if not set, depends on if we are at the end of a full epoch"""
        if self.__iteration_based_evaluation_active():
            return current_training_iter % self.cfg.train_iters_to_eval == 0
        else:
            return full_epoch_finished

    def train(self):
        train_dataloader = self.__load_train_dataloader()
        dev_dataloader = self.__load_dev_dataloader()
        global_iter_number = 0
        for epoch in tqdm(range(self.cfg.num_epochs), ascii=' ||||', position=0):
            global_iter_number = self.__do_train_epoch(train_dataloader=train_dataloader, dev_dataloader=dev_dataloader,
                                                       epoch=epoch, training_iteration=global_iter_number)

            if self.__should_evaluate(current_training_iter=global_iter_number, full_epoch_finished=True):
                self.__evaluation_and_saving(dev_dataloader=dev_dataloader, epoch=epoch, iteration=global_iter_number)
            if self.conditional_checkpoint_saver.is_early_stopping_patience_exhausted():
                logger.info(f' >> Early stopping patience of {self.cfg.early_stopping_patience} epochs exhausted. Stopping the training.')
                break

    def __do_train_epoch(self, train_dataloader: DataLoader, dev_dataloader: DataLoader, epoch: int, training_iteration: int) -> int:
        with batch_progress_bar(epoch=epoch, num_epochs=self.cfg.num_epochs, dataloader=train_dataloader) as t:
            for batch_number, batch in enumerate(t):
                metric_values = self.__train_step(batch_number, batch, epoch, iteration=training_iteration)
                report_to_progress_bar(progress_bar=t, metric_values=metric_values,
                                       metrics_for_progress_bar=self.metrics_holder.metrics_in_progress_bar,
                                       current_learning_rate=get_learning_rate(self.optimizer))

                training_iteration += 1
                if self.__should_evaluate(current_training_iter=training_iteration, full_epoch_finished=False):
                    self.__evaluation_and_saving(dev_dataloader=dev_dataloader, epoch=epoch, iteration=training_iteration)

        # return the update count of iterations
        return training_iteration

    def __train_step(self, batch_number: int, batch: Dict, epoch: int, iteration: int):
        self.model.train()

        model_step_output: ModelStepOutput = self.model_step(self.cfg, self.model, batch=batch, batch_number=batch_number)
        loss = model_step_output.loss
        prediction_scores = model_step_output.prediction_scores
        labels = model_step_output.gold_labels

        if self.is_multigpu_setting:
            loss = loss.mean()  # mean() to average on multi-gpu parallel training

        loss = loss / self.gradients_accumulation_steps  # Normalize our loss (if averaged)
        self.scaler.scale(loss).backward()

        if (batch_number + 1) % self.gradients_accumulation_steps == 0:
            if self.cfg.clip_grad_norm:
                self.__apply_gradient_clipping()
            # To try to avoid the "optimizer.step() before lr_scheduler.step()" warning
            # https://discuss.pytorch.org/t/optimizer-step-before-lr-scheduler-step-error-using-gradscaler/92930/8
            self.scaler.step(self.optimizer)
            scale = self.scaler.get_scale()
            self.scaler.update()
            skip_lr_scheduler_step = (scale != self.scaler.get_scale())
            self.optimizer.zero_grad()
            if not skip_lr_scheduler_step:
                self.lr_scheduler.step()

        metric_values = self.train_evaluation_metrics_accumulator.calculate_and_accumulate_metrics(gold_labels=labels,
                                                                                                   model_outcomes=prediction_scores,
                                                                                                   loss=loss.item())
        return metric_values

    def __apply_gradient_clipping(self):
        if self.cfg.clip_grad_norm and self.cfg.clip_grad_norm > 0.0:
            self.scaler.unscale_(self.optimizer)
            torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.cfg.clip_grad_norm)

    def __evaluation_and_saving(self, dev_dataloader, epoch, iteration):
        # Average metric values
        # train_metrics = self.train_evaluation_metrics_accumulator.get_all_metrics_average(reset_afterwards=report_as_epochs)
        # Evaluation
        dev_metrics = self.__evaluate(dev_dataloader, epoch, iteration)

        # Conditionally save a model checkpoint, and check for early stopping condition
        self.conditional_checkpoint_saver.check_and_store(model=self.model,
                                                          metrics={m_name: m_value for m_name, m_value in dev_metrics.items()},
                                                          epoch=epoch,
                                                          iteration=iteration, metrics_for_checkpoint=self.metrics_holder.metrics_in_checkpoint_name)

    def __evaluate(self, dataloader, epoch: int, iteration: int):
        logger.info(f'Evaluating the model after epoch {epoch}, iteration {iteration}...')
        self.model.eval()
        with torch.no_grad():
            with batch_progress_bar(epoch, self.cfg.num_epochs, dataloader, iteration) as t:
                for batch_number, batch in enumerate(t):

                    model_step_output: ModelStepOutput = self.model_step(self.cfg, self.model, batch=batch, batch_number=batch_number)
                    loss = model_step_output.loss
                    model_outcomes = model_step_output.prediction_scores
                    labels = model_step_output.gold_labels
                    if self.is_multigpu_setting:
                        loss = loss.mean()  # mean() to average on multi-gpu parallel training

                    self.dev_evaluation_metrics_accumulator.calculate_and_accumulate_metrics(labels, model_outcomes, loss.item())

            # average metric values
            averaged_metrics = self.dev_evaluation_metrics_accumulator.get_all_metrics_average(reset_afterwards=True)

        return averaged_metrics
