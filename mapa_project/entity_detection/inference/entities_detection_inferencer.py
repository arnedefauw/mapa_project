import time
from typing import List, Dict

import torch
from dataclasses import dataclass
from torch import Tensor

from torch.utils.data import DataLoader

from mapa_project.data_processing.mapa_hierarchy import MapaEntitiesHierarchy
from mapa_project.entity_detection.common.dataset_loading import TwoFlatLevelsDataset
from mapa_project.entity_detection.common.entity_detection_model import EnhancedTwoFlatLevelsSequenceLabellingModel
from mapa_project.entity_detection.common.misc_logic import AutocastModel, preinstantiation_stuff
from mapa_project.entity_detection.common.model_saving_loading import ModelLoader, ModelAndResources
from mapa_project.entity_detection.common.windowed_sequences_helper import WindowedSequencesGenerator


@dataclass
class MapaSeqLabellingOutput:
    tokens: List[str]
    level1_tags: List[str]
    level2_tags: List[str]


class MAPATwoFlatLevelsInferencer:

    def __init__(self, model_path: str, valid_seq_len: int, ctx_window_len: int, batch_size: int, cuda_device_num=-1,
                 model_class=EnhancedTwoFlatLevelsSequenceLabellingModel, mask_level2: bool = False, use_amp: bool = True,
                 force_level1_from_level2: bool = False):
        preinstantiation_stuff()
        model_loader = ModelLoader(model_class)
        model_and_resources: ModelAndResources = model_loader.load_checkpoint(model_path, load_tokenizer=True)
        self.model = model_and_resources.model
        self.tokens_field = model_and_resources.data_fields['tokens']
        self.level1_labels_field = model_and_resources.data_fields['level1_tags']
        self.level2_labels_field = model_and_resources.data_fields['level2_tags']

        self.mapa_entities = MapaEntitiesHierarchy.from_json(model_and_resources.extra_resources['mapa_entities'])

        self.cuda_device_num = cuda_device_num
        self.device = f'cuda:{cuda_device_num}' if torch.cuda.is_available() and cuda_device_num >= 0 else 'cpu'

        self.model.to(self.device)
        self.model = AutocastModel(self.model, use_amp=use_amp and 'cuda' in self.device)

        self.tokenizer = model_and_resources.tokenizer
        self.valid_seq_len = valid_seq_len
        self.ctx_window_len = ctx_window_len
        self.batch_size = batch_size

        self.mask_level2 = mask_level2
        if self.mask_level2:
            self.precomputed_level2_masks = self.precompute_level2_masks()
        self.force_level1_from_level2 = force_level1_from_level2

    def precompute_level2_masks(self):
        level2_out_idx = self.level2_labels_field.stoi(self.level2_labels_field.default_value)
        default_tensor = torch.tensor([level2_out_idx], dtype=torch.long)

        level2_masks = {
            level1_tag_idx: torch.zeros([len(self.level2_labels_field.vocab)]).scatter(dim=0, index=default_tensor, value=1) for level1_tag_idx in
            self.level1_labels_field.reverse_vocab.keys()}
        # Default value ('O') must be left, because when a L1 entity spans over stopwords (e.g. 24th of November) the 'O' must be eligible
        for L1, L2_values in self.mapa_entities.to_bio_dict().items():
            L2_values.add(self.level2_labels_field.default_value)
            if len(L2_values) != 0:
                level1_idx: int = self.level1_labels_field.stoi(L1)
                level2_values_idx: List[int] = [self.level2_labels_field.stoi(val) for val in L2_values]
                mask = torch.zeros([len(self.level2_labels_field.vocab)]) \
                    .scatter(dim=0, index=torch.tensor(level2_values_idx, dtype=torch.long), value=1)
                level2_masks[level1_idx] = mask
        # move all the mask tensors to the selected device
        level2_masks = {key: val.to(self.device) for key, val in level2_masks.items()}
        # insert the mask for L1='O', forcing L2 to be also 'O'
        level2_masks[self.level1_labels_field.stoi(self.level1_labels_field.default_value)] = \
            torch.zeros([len(self.level2_labels_field.vocab)]).scatter(dim=0, index=default_tensor, value=1).to(self.device)

        return level2_masks

    @torch.no_grad()
    def make_inference(self, pretokenized_input: List[List[str]]) -> List[MapaSeqLabellingOutput]:
        """ The input expects an arbitrary number of documents already pre-tokenized """

        # Workaround for when an empty sequence is fed here (to prevent mismatches at the end)
        positions_of_empty_seqs = [seq_num for seq_num, seq in enumerate(pretokenized_input) if len(seq) == 0]
        pretokenized_input = [seq for seq in pretokenized_input if len(seq) > 0]

        dataset = TwoFlatLevelsDataset.load_for_inference(pretokenized_input=pretokenized_input,
                                                          tokenizer=self.tokenizer,
                                                          valid_seq_len=self.valid_seq_len,
                                                          ctx_len=self.ctx_window_len,
                                                          input_field=self.tokens_field,
                                                          level1_tags_field=self.level1_labels_field,
                                                          level2_tags_field=self.level2_labels_field
                                                          )
        dataloader = DataLoader(
            dataset=dataset,
            batch_size=self.batch_size,
            shuffle=False, num_workers=0
        )

        all_seq_nums: List[int] = []
        all_token_inputs: List[Tensor] = []
        all_level1_labels: List[Tensor] = []
        all_level2_labels: List[Tensor] = []

        for batch_num, batch in enumerate(dataloader):
            start_time = time.time()
            print(f'Processing batch {batch_num} of {len(dataloader)}...')
            instance_nums: Tensor = batch[WindowedSequencesGenerator.INSTANCE_IDX]
            input_ids = batch[self.tokens_field.name].to(device=self.device)
            ctx_mask = batch[self.tokens_field.name + '_ctx_mask'].to(device=self.device)
            input_mask = batch[self.tokens_field.attn_mask_name()].to(device=self.device)

            outputs = self.model(input_ids=input_ids, attention_mask=input_mask, ctx_mask=ctx_mask)
            level1_tags_logits, level2_tags_logits = outputs[:2]

            level1_tags_probs, predicted_level1_tags_labels = torch.max(level1_tags_logits.softmax(dim=2), dim=2)
            # here is where the hierarchy masking should do its effect, based on the predicted level1_tag
            # print('predicted_level1_tags_labels.shape', predicted_level1_tags_labels.shape)
            # print('pred level1 tags [0]', predicted_level1_tags_labels[0])
            if self.mask_level2:
                level2_masks = torch.stack(dim=0, tensors=[torch.stack(dim=0, tensors=[self.precomputed_level2_masks[L1.item()]
                                                                                       for L1 in seq]) for seq in predicted_level1_tags_labels])

                masked_level2_tags_prob_dist = (level2_tags_logits * level2_masks).softmax(dim=2)
                level2_tags_probs, predicted_level2_tags_labels = torch.max(masked_level2_tags_prob_dist, dim=2)
            else:
                level2_tags_probs, predicted_level2_tags_labels = torch.max(level2_tags_logits.softmax(dim=2), dim=2)

            all_token_inputs.append(input_ids)
            all_level1_labels.append(predicted_level1_tags_labels)
            all_level2_labels.append(predicted_level2_tags_labels)

            all_seq_nums += instance_nums.tolist()

            end_time = time.time()
            print(f'Batch time: {end_time - start_time:.4f} seconds')

        rebuilt_level1_label_sequences = WindowedSequencesGenerator.rebuild_original_sequences(seq_indices=all_seq_nums,
                                                                                               windowed_sequences=torch.cat(all_level1_labels, dim=0),
                                                                                               left_ctx_len=self.ctx_window_len + 1,
                                                                                               right_ctx_len=self.ctx_window_len + 1,
                                                                                               pad_idx=None)

        rebuilt_level2_label_sequences = WindowedSequencesGenerator.rebuild_original_sequences(seq_indices=all_seq_nums,
                                                                                               windowed_sequences=torch.cat(all_level2_labels, dim=0),
                                                                                               left_ctx_len=self.ctx_window_len + 1,
                                                                                               right_ctx_len=self.ctx_window_len + 1,
                                                                                               pad_idx=None)

        level1_label_instances = [{self.level1_labels_field.name: [self.level1_labels_field.itos(label) for label in labels]}
                                  for labels in rebuilt_level1_label_sequences]
        level2_label_instances = [{self.level2_labels_field.name: [self.level2_labels_field.itos(label) for label in labels]}
                                  for labels in rebuilt_level2_label_sequences]
        merged_instances = [{**level1_label_instances[i], **level2_label_instances[i]} for i in range(len(level1_label_instances))]

        remapped_instances: List[Dict[str, List]] = dataset.tokens_remapper.remap(instances_to_remap=merged_instances)

        decoded_instances = [{self.tokens_field.name: instance[self.tokens_field.name],
                              self.level1_labels_field.name: instance[self.level1_labels_field.name],
                              self.level2_labels_field.name: instance[self.level2_labels_field.name]}
                             for instance in remapped_instances]

        # here, re-insert the empty sequences, just in case, to be compliant
        for empty_seq_position in positions_of_empty_seqs:
            # print('Inserting back an empty sequence in its original position')
            decoded_instances.insert(empty_seq_position,
                                     {self.tokens_field.name: [], self.level1_labels_field.name: [], self.level2_labels_field.name: []})

        self.__postprocess_line_breaks_and_x_(decoded_instances)
        [self.__fix_bio_tagging_issues_(instance[self.level1_labels_field.name]) for instance in decoded_instances]
        [self.__fix_bio_tagging_issues_(instance[self.level2_labels_field.name]) for instance in decoded_instances]
        if self.force_level1_from_level2:
            self.__force_level1_from_level2_(decoded_instances)
        result: List[MapaSeqLabellingOutput] = [MapaSeqLabellingOutput(tokens=inst[self.tokens_field.name],
                                                                       level1_tags=inst[self.level1_labels_field.name],
                                                                       level2_tags=inst[self.level2_labels_field.name]) for inst in decoded_instances]
        return result

    def __postprocess_line_breaks_and_x_(self, instances: List[Dict[str, List[str]]]):
        """
        A curious side effect is that line-break \n appear as if they were 0-span tokens, getting the label of the next token.
        Despite being harmless to some extent (the labels are not shifted), it is confusing.
        It could be dealt otherwise (what about modeling line-breaks in the model properly or totally dropping them...?), but for now,
        this simple post-processing should to the trick. At the same time, convert the X into O if any of them appear...
        """
        for instance in instances:
            tokens = instance[self.tokens_field.name]
            level1_tags = instance[self.level1_labels_field.name]
            level2_tags = instance[self.level2_labels_field.name]
            for i, token in enumerate(tokens):
                if token == '\n':
                    tokens[i] = '[LINE_BREAK]'
                    level1_tags[i] = 'O'
                    level2_tags[i] = 'O'
                if level1_tags[i] == 'X':
                    level1_tags[i] = 'O'
                    level2_tags[i] = 'O'

    @classmethod
    def __fix_bio_tagging_issues_(cls, labels: List[str]):
        for i, bio_label in enumerate(labels):
            if '-' in bio_label:
                bio, label = bio_label.split('-', maxsplit=1)
                if i == 0 or labels[i - 1] == 'O':
                    bio = 'B'
                labels[i] = f'{bio}-{label}'

    def __force_level1_from_level2_(self, instances: List[Dict[str, List[str]]]):
        # print('FORCING LEVEL1 FROM LEVEL2:', instances[0])
        for instance in instances:
            level1_tags = instance[self.level1_labels_field.name]
            level2_tags = instance[self.level2_labels_field.name]
            for i, level1_tag in enumerate(level1_tags):
                level2_tag = level2_tags[i]
                if level1_tag == 'O' and level2_tag != 'O':
                    print( "level 1 and level 2 are different, but ommiting fix due to errors in implementation" ) #AD
                    pass #AD
                    # this is the case in which level2 says "something" and level1 says "nothing"
                    # derive level1 from the predicted level2, to increase the recall (even if incorrect, at least the entity will get anonymized)
                    #level1_wo_bio = self.mapa_entities.get_level2_parent(level2_tag, input_is_bio_tagged=True) #AD
                    #level1_tags[i] = f'{level2_tag.split("-", maxsplit=1)[0]}-{level1_wo_bio}'  # assuming that every tag that is not O has '-' #AD
        # print('AFTER FORCING LEVEL1 FROM LEVEL2:', instances[0])
