"""
This is aimed to contain the logic related to pre-tokenization, i.e. the tokenization before the actual transformer-model-based tokenization.
We need it mainly for inference, so we can give the model good enough pre-tokens to attach predictions to, but having calculated the actual
character offsets in advance, together with whichever other things, like line-breaks to preserve the text structure if needed.
"""
import logging
import re
from typing import List, Tuple, Union, Dict, ClassVar

from tqdm import tqdm
from transformers import PreTrainedTokenizer

logger = logging.getLogger(__name__)

CharSpan = Tuple[int, int]
TokenList = List[str]


class GeneralPreTokenizer:
    LINE_BREAK: ClassVar[str] = '[LINE_BREAK]'

    def __init__(self, prepend_whitespaces: bool, remove_linebreaks: bool, add_linebreak_tokens: bool):
        """
        Instantiates the simple tokenizer choosing a whitespace strategy
        :type remove_linebreaks: whether to remove or preserve the line breaks as tokens (does not affect the offset calculation)
        :param prepend_whitespaces: whether to remove or preserve whitespace, prepended to the following token, e.g. "a test" -> ['a', ' test']
        """
        self.prepend_whitespaces = prepend_whitespaces
        self.remove_linebreaks = remove_linebreaks
        self.add_linebreak_tokens = add_linebreak_tokens
        if self.remove_linebreaks and self.add_linebreak_tokens:
            raise Exception('add_line_break_tokens activated together with remove_linebreaks; is this a mistake?')
        self.p1 = re.compile(r'(\s)')
        self.p2 = re.compile(r'([^\w])')
        self.p3 = re.compile(r'(?:_SEPARATOR_)+')

    def __tokenize(self, text: str) -> List[str]:
        trans = re.sub(self.p1, r'_SEPARATOR_\1_SEPARATOR_', text)
        trans = re.sub(self.p2, r'_SEPARATOR_\1_SEPARATOR_', trans)
        trans = re.sub(self.p3, '_SEPARATOR_', trans)
        tokens: List[str] = trans.split('_SEPARATOR_')
        tokens = [token for token in tokens if token != '']
        if self.remove_linebreaks:
            tokens = [token for token in tokens if token != '\n']
        elif self.add_linebreak_tokens:
            tokens = [token if token != '\n' else self.LINE_BREAK for token in tokens]
        if self.prepend_whitespaces:
            new_tokens = []
            for i, token in enumerate(tokens):
                if token == self.LINE_BREAK:
                    new_tokens.append(token)
                elif token != ' ':
                    if i > 0 and tokens[i - 1] == ' ':
                        new_tokens.append(f' {token}')
                    else:
                        new_tokens.append(token)
            tokens = new_tokens
        else:
            # remove whitespaces
            tokens = [token for token in tokens if token != ' ']
        return tokens

    def __calculate_offsets(self, raw_text, tokens) -> List[CharSpan]:
        token_spans: List[Tuple[int, int]] = []
        latest_position = 0
        for token in tokens:
            if token == self.LINE_BREAK:
                token_spans.append((-1, -1))  # need to add a placeholder for this token to keep the numbers even. Need to reprocess after (see below)
            token = token.strip()
            for match in re.finditer(re.escape(token), raw_text[latest_position:]):
                span_start = latest_position + match.start()
                span_end = span_start + len(token)
                token_spans.append((span_start, span_end))
                latest_position = token_spans[-1][0] + len(token)
                break
        # reprocessing LINE_BREAK spans, they should cover from the end of the previous token, to the start of the next token
        for i, (start, end) in enumerate(token_spans):
            if start == -1 and end == -1:
                new_start = token_spans[i - 1][1] if i > 0 else 0
                # new_end = token_spans[i + 1][0] if i < len(token_spans) - 1 else len(raw_text)
                # the previous approach gave problems with more than one consecutive linebreak, assume len=1 (\n)
                # NOTE: assuming that linebreaks are just newline characters \n not carriage return \r (that can be pre/post-processed
                new_end = new_start + 1
                token_spans[i] = (new_start, new_end)
        return token_spans

    def do_pretokenization(self, raw_text: str,
                           calculate_offsets: bool,
                           as_list_of_tuples: bool = True) -> Union[List[str], List[Tuple[str, CharSpan]], Tuple[List[str], List[CharSpan]]]:
        tokens: List[str] = self.__tokenize(raw_text)
        if calculate_offsets:
            token_spans = self.__calculate_offsets(raw_text, tokens)
            if as_list_of_tuples:
                tokens_with_spans: List[Tuple[str, CharSpan]] = list(zip(tokens, token_spans))
                return tokens_with_spans
            else:
                return tokens, token_spans
        else:
            return tokens


class TokenizationRemappingHelper:
    """
    A helper class for the re-tokenization. It is a bit subtle, because it needs to receive the equivalent retokenized instances
    to perform the remapping. That means, the same number (and order) of instances, with the same fields, and without special tokens (CLS, SEP, etc.).
    So basically the sequences must be cleaned and reconstructed back to what they were before the reverse process.
    """

    def __init__(self, pretokenized_instances: List[Dict[str, List[str]]], tokenizer: PreTrainedTokenizer,
                 tokens_field_name: str, tag_fields_names: Tuple[str, ...], subword_tag: str = 'X'):
        self.tokenizer = tokenizer
        self.tokens_field_name = tokens_field_name
        self.tag_fields_names = tag_fields_names
        self.original_tokens: List[List[str]] = [instance[tokens_field_name] for instance in pretokenized_instances]
        self.subword_tag = subword_tag
        self.retokenized_instances, self.orig_to_tok_mappings = self.__retokenize_and_remap(pretokenized_instances)

    def __retokenize_and_remap(self, pretokenized_instances: List[Dict[str, List[str]]]):
        # Note that in this remapping the special tokens are not accounted
        # The mapping will work after cleaning back the tokens, only the actual tokens of the valid sequence part
        retokenized_instances = []
        all_instances_orig_to_tok_map = []
        logger.info('Re-tokenizing using the chosen Transformers tokenizer and calculating labels remapping...')
        for instance in tqdm(pretokenized_instances):
            orig_tokens: List[str] = instance[self.tokens_field_name]
            orig_tags_collection: List[List[str]] = [instance[tags_f_name] for tags_f_name in self.tag_fields_names]
            target_tokens: List[str] = []
            target_tags_collection: List[List[str]] = [[] for _ in range(len(orig_tags_collection))]
            orig_to_tok_map = []
            for i, token in enumerate(orig_tokens):
                orig_to_tok_map.append(len(target_tokens))
                current_target_tokens = self.tokenizer.tokenize(token)
                if len(current_target_tokens) == 0:
                    continue
                target_tokens += current_target_tokens
                for orig_tags_num, orig_tags in enumerate(orig_tags_collection):
                    target_tags_collection[orig_tags_num] += [orig_tags[i]] + [self.subword_tag] * (len(current_target_tokens) - 1)

            retokenized_instances.append(
                {self.tokens_field_name: target_tokens, **{tags_f_name: target_tags_collection[num]
                                                           for num, tags_f_name in enumerate(self.tag_fields_names)}})
            all_instances_orig_to_tok_map.append(orig_to_tok_map)
        return retokenized_instances, all_instances_orig_to_tok_map

    def remap(self, instances_to_remap: List[Dict[str, List[str]]], fields_to_ignore: List[str] = None) -> List[Dict[str, List[str]]]:
        """
        Upon receiving the collection of retokenized instances that was used to create the object, remaps the tokens/labels back
        :param instances_to_remap: the retokenized instances that correspond the output of the instantiation
        :param fields_to_ignore: a list of field names that will be ignored for the remapping (and thus not present in the output)
        :return: the back-tokenized instances after applying the orig_to_tok mapping to each of them
        """
        remapped_instances = []
        for i, instance in enumerate(instances_to_remap):
            try:
                remapped_instance = {self.tokens_field_name: self.original_tokens[i]}
                for field in list(self.tag_fields_names):
                    if fields_to_ignore and field in fields_to_ignore:
                        continue
                    sequence = instance[field]
                    remapped_instance[field] = [sequence[idx] for idx in self.orig_to_tok_mappings[i]]
                remapped_instances.append(remapped_instance)
            except IndexError as e:
                logger.error('Some instance has given remapping problems, returning an empty instance', exc_info=e)
                remapped_instances.append({self.tokens_field_name: [], **{tag_field_name: [] for tag_field_name in self.tag_fields_names}})
        return remapped_instances
