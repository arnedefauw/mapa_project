"""
Store/load models and all their related resources.
Note that, apart from the general case of unbounded number of vocabularies, there may be additional resources.
Take into account (and try to anticipate) all those requirements, but keep the things clear
"""
import json
import logging
import os
import shutil
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, Any, Type, ClassVar, List, Set

from transformers import PreTrainedTokenizer, AutoTokenizer, PreTrainedModel
from torch.nn import Module

from mapa_project.entity_detection.common.field_definition import FieldDefinition
from mapa_project.entity_detection.training.entity_detection_trainer_config import MapaEntityDetectionTrainerConfig
from mapa_project.entity_detection.training.training_evaluation import BaseEvaluationMetric

EXTRA_RESOURCE_APPENDIX = '_extra_resource'


class ModelSaver:
    logger: ClassVar[logging.Logger] = logging.getLogger(__name__)

    def __init__(self, base_folder, model_name: str, model_version: int, data_fields: Dict[str, FieldDefinition],
                 extra_resources: Dict[str, Any], training_config: MapaEntityDetectionTrainerConfig,
                 tokenizer: PreTrainedTokenizer = None):
        """
        Inits the model saver with the values that will remain unchanged among the several 'save_checkpoint' calls
        :param base_folder: the folder to which save the model and its additional resources
        :param model_name: the name of the model
        :param data_fields: a dictionary of names and vocabularies for the target variables
        :param extra_resources: a dictionary of names and the corresponding extra resources (ad-hoc for each task/model)
        """
        self.base_folder = base_folder
        self.model_name = model_name
        self.model_version = model_version
        self.data_fields: Dict[str, FieldDefinition] = data_fields
        self.extra_resources = extra_resources
        self.training_config_as_dict = {x: str(y) if isinstance(y, Path) else y for x, y in vars(training_config).items()}
        self.tokenizer: PreTrainedTokenizer = tokenizer

    def save_checkpoint(self, model: PreTrainedModel, epoch: int, iteration: int, metrics: Dict[str, float]) -> str:
        """
        Save a checkpoint of the current model
        :param model: the model to be saved
        :param epoch: the epoch number
        :param iteration: the training iteration number (how many times the model has back-propagated)
        :param metrics: dictionary of names and values of the evaluated metrics (will appear in the name)
        :return:
        """
        model_folder_name = self.__compose_model_folder_name(epoch=epoch, iteration=iteration, metrics=metrics)
        model_folder = self.__check_and_obtain_model_folder_path(model_folder_name)
        self.logger.info(f'Saving model to {os.path.abspath(model_folder)}')
        model.save_pretrained(save_directory=model_folder)
        if self.tokenizer:
            self.logger.info('Saving tokenizer together with the model')
            self.tokenizer.save_pretrained(model_folder)
        if self.data_fields:
            self.__store_data_fields(model_folder)
        if self.extra_resources:
            self.__store_extra_resources(model_folder)
        self.__store_configuration_info(model_folder)
        # return the full path to the model just saved
        return os.path.abspath(model_folder)

    def __check_and_obtain_model_folder_path(self, model_folder_name):
        model_folder = os.path.join(self.base_folder, model_folder_name)
        if not os.path.exists(model_folder):
            os.makedirs(model_folder)
        return model_folder

    def __compose_model_folder_name(self, epoch, iteration, metrics: Dict[str, float]):
        composed_name = f'{self.model_name}_v{self.model_version}_epoch{epoch}_iter{iteration}'  # _F1-{f1}_binF1-{bin_f1}'
        for x, y in metrics.items():
            composed_name += f'_{x}-{y:1.4f}'  # only one position for non-decimals, assuming a metric in range [0,1]...
        return composed_name

    def __store_data_fields(self, model_folder):
        self.logger.info(f'Saving vocabularies: {self.data_fields.keys()}')
        for vocab_name, data_field in self.data_fields.items():
            data_field.serialize_to_file(base_path=model_folder, field_name=vocab_name)

    def __store_extra_resources(self, model_folder):
        # Despite it is not enforced, hopefully the extra resources are Dicts, or at most, Lists
        self.logger.info(f'Saving extra_resources: {self.extra_resources.keys()}')
        for resource_name, resource in self.extra_resources.items():
            path_for_the_resource = os.path.join(model_folder, f'{resource_name}{EXTRA_RESOURCE_APPENDIX}.json')
            with open(path_for_the_resource, 'w', encoding='utf-8') as f:
                self.logger.info(f'Saving the extra resource {resource_name} to: {path_for_the_resource}')
                json.dump(resource, f)

    def __store_configuration_info(self, model_folder):
        self.logger.debug(f'Saving provided training configuration together with the model checkpoint...')
        path = os.path.join(model_folder, 'training_config.json')
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(self.training_config_as_dict, f, indent=4)


@dataclass
class ModelAndResources:
    model: Module
    data_fields: Dict[str, FieldDefinition] = None
    extra_resources: Dict[str, Any] = None
    tokenizer: PreTrainedTokenizer = None

    # Added explicitly to fix the problem with Cython and dataclasses (that eventually will get solved)
    # See: https://stackoverflow.com/questions/56079419/using-dataclasses-with-cython
    __annotations__ = {
        'model': Module,
        'data_fields': Dict[str, FieldDefinition],
        'extra_resources': Dict[str, Any],
        'tokenizer': PreTrainedTokenizer
    }


class ModelLoader:
    logger: ClassVar[logging.Logger] = logging.getLogger(__name__)

    """
    Few comments of this (Model Loading). The model_class could be saved with the model (at least the name of some cue...)
    Even better, I think that the model class should be serialized, but not meant to te directly loaded back.
    I mean, there should be a way to ensure the consistency (for those cases in which the model architecture is inadvertently changed or gets lost
    That way the signature of the class (its content/components) could be compared, and even recovered (by manual inspection...?)
    I am almost sure that a plain serialization will given problems, and I do not want it... I want a simple way to compare the relevant parts...
    Maybe the "name", or vars, or __dict__ or... I don't know  (I think that Module.modules() is the answer) (together with model's given name)
    https://discuss.pytorch.org/t/module-children-vs-module-modules/4551
    """

    """
    And w.r.t optimizer/lr_state, we would need to store both the class, the state_dict and the desired configuration
    (eih, interestingly I could compare how heavy optimizers are by storing the state_dict...)
    """

    def __init__(self, model_class: Type[PreTrainedModel]):
        self.model_class = model_class

    def load_checkpoint(self, model_path, load_tokenizer=False) -> ModelAndResources:
        tokenizer = None
        if load_tokenizer:
            self.logger.info('Loading tokenizer together with the model (assuming that tokenizer data was stored with it)')
            tokenizer = AutoTokenizer.from_pretrained(model_path)
        else:
            self.logger.info('load_tokenizer=False, you will need to load the corresponding tokenizer externally')
        model = self.model_class.from_pretrained(model_path)

        data_fields = self.__load_data_fields(model_path)
        extra_resources = self.__load_extra_resources(model_path)

        model_and_resources = ModelAndResources(model=model, data_fields=data_fields, extra_resources=extra_resources, tokenizer=tokenizer)
        return model_and_resources

    def __load_data_fields(self, model_path) -> Dict[str, FieldDefinition]:
        data_fields: Dict[str, FieldDefinition] = {}
        stored_fields = [filename for filename in os.listdir(model_path) if FieldDefinition.STORE_NAME_APPENDIX in filename]
        for stored_field_file_name in stored_fields:
            # check whether the model has pickle or json serialized vocabulary (for backwards compatibility)
            field_name = stored_field_file_name.split(FieldDefinition.STORE_NAME_APPENDIX)[0]
            if os.path.exists(os.path.join(model_path, stored_field_file_name)):
                stored_field = FieldDefinition.deserialize_from_file(base_path=model_path,
                                                                     field_name=field_name)
                data_fields[field_name] = stored_field
            else:
                self.logger.warning(f'Field {field_name} could not be loaded, does it exists?')
        return data_fields

    def __load_extra_resources(self, model_path) -> Dict[str, Any]:
        extra_resources: Dict[str, Any] = {}
        extra_resource_files = [filename for filename in os.listdir(model_path) if EXTRA_RESOURCE_APPENDIX in filename]
        for extra_resource_file in extra_resource_files:
            extra_resource_name = extra_resource_file.split(EXTRA_RESOURCE_APPENDIX)[0]
            resource_path = os.path.join(model_path, extra_resource_file)
            if os.path.exists(os.path.join(model_path, extra_resource_file)):
                with open(resource_path, 'r', encoding='utf-8') as f:
                    resource = json.load(f)
                    extra_resources[extra_resource_name] = resource
            else:
                self.logger.warning(f'Extra resource {extra_resource_name} could not be loaded, does it exists?')
        return extra_resources


class ConditionalCheckpointSaver:
    logger: ClassVar[logging.Logger] = logging.getLogger(__name__)

    def __init__(self, model_saver: ModelSaver, conditioning_metric_name: str, maximize_metric: bool = True,
                 early_stopping_patience: int = None, max_checkpoints_to_keep: int = None):
        self.model_saver = model_saver
        self.conditioning_metric_name = conditioning_metric_name
        self.maximize_metric = maximize_metric
        self.early_stopping_patience = early_stopping_patience
        # state relate variables
        self.best_value_so_far = None
        self.step_with_best_value = None
        self.patience_exhausted = False

        ######
        # Fields to keep track of the stored models (in this run) for example to remove the oldest ones
        self.saved_checkpoints_path: List[str] = []
        self.max_checkpoints_to_keep: int = max_checkpoints_to_keep

    def check_and_store(self, model, metrics: Dict[str, float], epoch, iteration, metrics_for_checkpoint: List[BaseEvaluationMetric]):
        metrics_for_checkpoint_names: Set[str] = {metric.name for metric in metrics_for_checkpoint}
        if self.best_value_so_far is None:
            # first time ever, no best-value yet, store model and keep track of the best value
            saved_checkpoint_path = self.model_saver.save_checkpoint(model=model, epoch=epoch, iteration=iteration,
                                                                     metrics={m: v for m, v in metrics.items() if m in metrics_for_checkpoint_names})
            self.saved_checkpoints_path.append(saved_checkpoint_path)
            self.best_value_so_far = metrics[self.conditioning_metric_name]
            self.step_with_best_value = epoch
        else:
            # compare the best value and act in consequence
            conditioning_metric_value = metrics[self.conditioning_metric_name]
            metric_has_improved = self.__check_if_metric_has_improved(conditioning_metric_value)
            if metric_has_improved:
                self.logger.info(
                    f'Better value for {self.conditioning_metric_name},'
                    f' previous was:{self.best_value_so_far} (from epoch:{self.step_with_best_value})'
                    f', new one is: {conditioning_metric_value} at epoch {epoch} (iter {iteration})'
                    f' (metric-diff:{conditioning_metric_value - self.best_value_so_far} ; epoch diff:{epoch - self.step_with_best_value})')
                self.best_value_so_far = conditioning_metric_value
                self.step_with_best_value = epoch
                saved_checkpoint_path = self.model_saver.save_checkpoint(model=model, epoch=epoch, iteration=iteration,
                                                                         metrics={m: v for m, v in metrics.items() if
                                                                                  m in metrics_for_checkpoint_names})
                self.saved_checkpoints_path.append(saved_checkpoint_path)
            self.__check_and_remove_oldest_checkpoints()

        # now check if current step is beyond patience w.r.t. the last best_model step
        if self.early_stopping_patience:
            self.patience_exhausted = self.__check_if_patience_has_been_exhausted(current_step=epoch)

    def __check_and_remove_oldest_checkpoints(self):
        if self.max_checkpoints_to_keep and len(self.saved_checkpoints_path) > self.max_checkpoints_to_keep:
            checkpoints_to_remove = self.saved_checkpoints_path[:-self.max_checkpoints_to_keep]
            checkpoints_to_retain = self.saved_checkpoints_path[-self.max_checkpoints_to_keep:]
            self.logger.info(f'Retaining only the latest {self.max_checkpoints_to_keep} checkpoints, removing the older ones...')
            for chpk_to_remove in checkpoints_to_remove:
                self.logger.info(f'Removing old checkpoint: {checkpoints_to_remove}')
                # don't fail in case something prevents the removal (e.g. manually inspecting a checkpoint folder at the instant it was to be deleted)
                shutil.rmtree(chpk_to_remove, ignore_errors=True)
            self.saved_checkpoints_path = checkpoints_to_retain

    def __check_if_patience_has_been_exhausted(self, current_step):
        return (current_step - self.step_with_best_value) > self.early_stopping_patience

    def __check_if_metric_has_improved(self, conditioning_metric_value: float):
        if self.maximize_metric:
            return conditioning_metric_value > self.best_value_so_far
        else:
            return conditioning_metric_value < self.best_value_so_far

    def is_early_stopping_patience_exhausted(self):
        return self.patience_exhausted
