"""
The TwoFlatLevels model for MAPA.
It is a sequence-labelling (i.e. token classification) model with two classification heads, one for level 1 (coarse-grained) entities, and
another for level 2 (more fine-grained) entities.
The model is also developed to admit the sliding-windows with context training, allowing a context mask to avoid training on context positions.
"""
import torch
from torch import nn
from torch.nn import CrossEntropyLoss
from transformers.models.bert import BertPreTrainedModel, BertModel, BertConfig


class EnhancedTwoFlatLevelsSequenceLabellingConfig(BertConfig):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # to be set after init (less elegant, but to avoid tampering with the from_pretrained function from HF)
        self.num_level2_labels = None
        self.num_level1_labels = None


class TokenPooler(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.dense = nn.Linear(config.hidden_size, config.hidden_size)
        self.activation = nn.Tanh()

    def forward(self, hidden_states):
        pooled_output = self.dense(hidden_states)
        pooled_output = self.activation(pooled_output)
        return pooled_output


class EnhancedTwoFlatLevelsSequenceLabellingModel(BertPreTrainedModel):
    """ Derived from and inspired by the BertForTokenClassification class from the (awesome!) HuggingFace Transformers library """

    def __init__(self, config: EnhancedTwoFlatLevelsSequenceLabellingConfig):
        super().__init__(config)
        self.num_level1_labels = config.num_level1_labels
        self.num_level2_labels = config.num_level2_labels

        self.bert = BertModel(config, add_pooling_layer=False)

        self.level1_labels_pooler = TokenPooler(config)
        self.level2_labels_pooler = TokenPooler(config)

        # About stateless layers reuse, like non-linear or dropout: TL;DR: avoid reusing, instantiate as many objects as necessary
        # https://discuss.pytorch.org/t/using-same-dropout-object-for-multiple-drop-out-layers/39027/6
        self.level1_labels_dropout = nn.Dropout(config.hidden_dropout_prob)
        self.level2_labels_dropout = nn.Dropout(config.hidden_dropout_prob)
        self.level1_labels_classifier = nn.Linear(config.hidden_size, self.num_level1_labels)
        self.level2_labels_classifier = nn.Linear(config.hidden_size, self.num_level2_labels)

        self.level1_loss_fct = CrossEntropyLoss()
        self.level2_loss_fct = CrossEntropyLoss()

        self.init_weights()

    def forward(
            self,
            input_ids=None,
            attention_mask=None,
            ctx_mask=None,
            token_type_ids=None,
            position_ids=None,
            head_mask=None,
            inputs_embeds=None,
            level1_labels=None,
            level2_labels=None,
            output_attentions=None,
            output_hidden_states=None
    ):

        outputs = self.bert(
            input_ids,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
            position_ids=position_ids,
            head_mask=head_mask,
            inputs_embeds=inputs_embeds,
            output_attentions=output_attentions,
            output_hidden_states=output_hidden_states,
        )

        sequence_output = outputs[0]

        level1_labels_pooled = self.level1_labels_pooler(sequence_output)
        level1_labels_pooled = self.level1_labels_dropout(level1_labels_pooled)
        level1_labels_logits = self.level1_labels_classifier(level1_labels_pooled)

        level2_labels_pooled = self.level2_labels_pooler(sequence_output)
        level2_labels_pooled = self.level2_labels_dropout(level2_labels_pooled)
        level2_labels_logits = self.level2_labels_classifier(level2_labels_pooled)

        loss = None
        if level1_labels is not None:  # assuming that level2_labels come along
            # Only keep active parts of the loss
            if attention_mask is not None:  # assuming that the ctx_mask will come along with the attention_mask ALWAYS
                # Combine the ctx_mask with the attention_mask for the loss calculation
                active_loss = (attention_mask.view(-1) * ctx_mask.view(-1)) == 1
                active_level1_labels_logits = level1_labels_logits.view(-1, self.num_level1_labels)
                active_level2_labels_logits = level2_labels_logits.view(-1, self.num_level2_labels)
                active_level1_labels = torch.where(
                    active_loss, level1_labels.view(-1), torch.tensor(self.level1_loss_fct.ignore_index).type_as(level1_labels)
                )
                active_level2_labels = torch.where(
                    active_loss, level2_labels.view(-1), torch.tensor(self.level2_loss_fct.ignore_index).type_as(level2_labels)
                )
                level1_labels_loss = self.level1_loss_fct(active_level1_labels_logits, active_level1_labels)
                level2_labels_loss = self.level2_loss_fct(active_level2_labels_logits, active_level2_labels)
            else:
                level1_labels_loss = self.level1_loss_fct(level1_labels_logits.view(-1, self.num_level1_labels), level1_labels.view(-1))
                level2_labels_loss = self.level2_loss_fct(level2_labels_logits.view(-1, self.num_level2_labels), level2_labels.view(-1))

            loss = level1_labels_loss + level2_labels_loss

        # I have removed the HuggingFace dictionary stuff, unfortunately it seems to be designed for a single bunch of logits...
        output = (level1_labels_logits, level2_labels_logits,)  # + outputs[2:]
        return ((loss,) + output) if loss is not None else output

    # just to comply with the abstract parent requirements
    def _reorder_cache(self, past, beam_idx):
        pass
