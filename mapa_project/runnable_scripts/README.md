### NOTE:

The files in this folder are some ad-hoc scripts used during development to run or check some things.

They are no general tools, and are not expected to be used (unless you know what they are about).
Simply ignore them :)