##########################
# VERSION INFO
##########################
import logging

version_info = (1, 0, 8)
__version__ = '.'.join(str(c) for c in version_info)


def init_logger():
    FORMAT = '%(asctime)s %(levelname)s (%(threadName)s) %(module)s: %(message)s'
    logging.basicConfig(format=FORMAT, level=logging.INFO)


init_logger()
logger = logging.getLogger(__name__)

logger.info(f'Launching MAPA version: {__version__}')
