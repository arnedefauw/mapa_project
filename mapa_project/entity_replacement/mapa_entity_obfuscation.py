"""
A simple obfuscation, replacing actual words by '*' symbols
"""
import logging
from dataclasses import dataclass
from typing import List

from confuse import Configuration

from mapa_project.anonymization.datatypes_definition import Tokens, Labels
from mapa_project.anonymization.helpers import strip_bio_tagging
from mapa_project.entity_detection.common.pretokenization_helpers import CharSpan

logger = logging.getLogger(__name__)


@dataclass
class EntityObfuscationConfig:
    entities_to_obfuscate: List[str]

    @classmethod
    def load_from_config(cls, config: Configuration) -> 'EntityObfuscationConfig':
        return EntityObfuscationConfig(entities_to_obfuscate=config['replacement']['obfuscation']['entities_to_obfuscate'].get(list))


class MapaEntityObfuscator:

    def __init__(self, config: EntityObfuscationConfig):
        self.config = config
        logger.info(f'Configured entities to obfuscate: {config.entities_to_obfuscate}')

    def update_operational_config(self, config: Configuration):
        """ A method that further propagates (or applies) the configuration changes that can be updated on-the-fly """
        self.config = EntityObfuscationConfig.load_from_config(config)

    def obfuscate_text(self, text: str, tokens: Tokens, spans: List[CharSpan], level1_labels: Labels, level2_labels: Labels) -> str:
        obfuscated_text_chars = list(text)
        for i, token in enumerate(tokens):
            level1_label = strip_bio_tagging(level1_labels[i])
            level2_label = strip_bio_tagging(level2_labels[i])
            span = spans[i]
            if level1_label in self.config.entities_to_obfuscate or level2_label in self.config.entities_to_obfuscate:
                logger.info(f'Obfuscating (l1_label:{level1_label} l2_label:{level2_label})')
                obfuscated_text_chars[span[0]:span[1]] = '*' * len(token)
        return ''.join(obfuscated_text_chars)
