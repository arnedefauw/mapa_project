import html
import logging
import os
import time
from typing import List

from fastapi import APIRouter, Form
from fastapi.encoders import jsonable_encoder
from starlette.requests import Request
from starlette.responses import HTMLResponse
from starlette.templating import Jinja2Templates

from mapa_project.anonymization.datatypes_definition import MapaResult
from mapa_project.webservice.app.brat_format_conversion import BratFormatProcessor, generate_brat_entity_types
from mapa_project.webservice.app.web_api_formats import DeployedModelsInfo, AnonymizationResponse, AnalysisInfo, AnonymizationRequest, \
    AnonymizationOp, Annotation
from mapa_project.webservice.loaded_components_managing import load_or_get_components_manager

logger = logging.getLogger(__file__)

brat_entity_types = generate_brat_entity_types()


def generate_label_types_html_legend():
    html_classes = ' '.join(
        [f'<span class="badge badge-light"><span class="badge badge-pill" style="background-color:{brat_entity_type.bgColor}">&nbsp;</span>'
         f'&nbsp;{html.escape(brat_entity_type.type)}</span>' for
         brat_entity_type in brat_entity_types])
    return html_classes


mapa_web_service_router = APIRouter()
templates = Jinja2Templates(directory=os.path.join(os.path.dirname(__file__), os.path.pardir, "templates"))

STATIC_FILES_PATH = os.path.join(os.path.dirname(__file__), os.pardir, "static")

DEMO_TEMPLATE_NAME = 'demo_new_layout.html'


@mapa_web_service_router.get("/model_showcase", response_class=HTMLResponse, include_in_schema=False)
async def mapa_showcase_home(request: Request):
    return templates.TemplateResponse(DEMO_TEMPLATE_NAME, {"request": request, "input_text": load_or_get_components_manager().default_demo_text,
                                                           "detected_classes": generate_label_types_html_legend(),
                                                           "available_models": list_deployed_models()})


@mapa_web_service_router.post("/model_showcase", response_class=HTMLResponse, include_in_schema=False)
async def mapa_showcase_analysis(request: Request, text: str = Form(...), model_label: str = Form(None),
                                 # obfuscate_output: Optional[bool] = Form(False), replace_entities: Optional[bool] = Form(False)
                                 anon_op: str = Form(None)
                                 ):
    obfuscate_output = anon_op == 'obfuscate'
    replace_entities = anon_op == 'replace'
    obfuscate_output_status = 'checked' if obfuscate_output else ''
    replace_entities_status = 'checked' if replace_entities else ''
    model_label = model_label or load_or_get_components_manager().default_anonymizer_label
    logger.info(f'Using model label: {model_label}')
    # anon_service = _nerc_services[model_label]
    anonymizer = load_or_get_components_manager().get_anonymizer(model_label)
    if text:
        start_time = time.time()

        anonymization_op = AnonymizationOp.REPLACE if replace_entities else AnonymizationOp.OBFUSCATE if obfuscate_output else AnonymizationOp.NOOP

        print(f'ANONYMIZATION OP: {anonymization_op}, derived from anon_op:{anon_op}')

        mapa_result: MapaResult = anonymizer.anonymize(text=text, anonymization_op=anonymization_op)
        # Obfuscation is now controlled by the anonymizer, BratProcessor does not need to obfuscate anything (setting its flag to False)
        brat_processor = BratFormatProcessor(entity_types=brat_entity_types, mapa_result=mapa_result, obfuscate_entities=False)
        brat_doc = brat_processor.brat_document
        brat_coll = brat_processor.brat_formatting

        # print('BRAT_DOC:', brat_doc)

        end_time = time.time()
        analysis_time = end_time - start_time

        analysis_info = AnalysisInfo(num_tokens=mapa_result.num_tokens, elapsed_time=mapa_result.analysis_seconds,
                                     used_device='GPU' if mapa_result.using_gpu else 'CPU')

        json_compatible_brat_coll = jsonable_encoder(brat_coll)
        json_compatible_brat_doc = jsonable_encoder(brat_doc)

        output_map = {"request": request, "input_text": text, "collData": json_compatible_brat_coll, "docData": json_compatible_brat_doc,
                      "analysis_time": f'{analysis_time:.3f}', "detected_classes": generate_label_types_html_legend(),
                      "obfuscate_output": obfuscate_output_status, "analysis_info": analysis_info, "available_models": list_deployed_models(),
                      "selected_model": model_label, "replace_entities": replace_entities_status}
    else:
        output_map = {"request": request, "input_text": text, "collData": '', "docData": '',
                      "analysis_time": f'{0.:.3f}', "detected_classes": generate_label_types_html_legend(),
                      "obfuscate_output": obfuscate_output_status, "available_models": list_deployed_models(), "selected_model": model_label,
                      "replace_entities": replace_entities_status
                      }

    return templates.TemplateResponse(DEMO_TEMPLATE_NAME, output_map)


@mapa_web_service_router.post("/anonymize_text", response_model=AnonymizationResponse)
def anonymize_text(anonymization_request: AnonymizationRequest):
    text = anonymization_request.text if anonymization_request.text else ''
    model_label = anonymization_request.model_label or load_or_get_components_manager().default_anonymizer_label
    operation: AnonymizationOp = anonymization_request.operation if anonymization_request.operation else AnonymizationOp.NOOP

    logger.info(f'Processing text: {text[:20]}[...] (model:{model_label}, operation:{operation})')

    start_time = time.time()
    num_whitespace_tokens = len(text.split())
    anon_service = load_or_get_components_manager().get_anonymizer(model_label)
    mapa_result: MapaResult = anon_service.anonymize(text=text, anonymization_op=operation)
    end_time = time.time()
    annotation_time = end_time - start_time
    words_per_second = num_whitespace_tokens / annotation_time
    entities = sorted(mapa_result.level1_entities + mapa_result.level2_entities, key=lambda x: x.offset)
    annotation_response = AnonymizationResponse(anonymized_text=mapa_result.text,
                                                annotations=[Annotation.from_mapa_entity(entity) for entity in entities],
                                                elapsed_seconds=annotation_time,
                                                words_per_second=words_per_second,
                                                used_device='GPU' if mapa_result.using_gpu else 'CPU')
    return annotation_response


@mapa_web_service_router.get("/list_models", response_model=List[DeployedModelsInfo])
def list_deployed_models():
    deployed_model_labels = load_or_get_components_manager().loaded_anonymizers.keys()
    deployed_models_response = [DeployedModelsInfo(model_label=m, is_default=m == load_or_get_components_manager().default_anonymizer_label) for m in
                                deployed_model_labels]
    return deployed_models_response
